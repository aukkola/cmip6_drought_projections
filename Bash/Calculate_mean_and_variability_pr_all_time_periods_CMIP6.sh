#!/bin/bash

#PBS -P dt6
#PBS -l walltime=2:00:00
#PBS -l mem=5000MB
#PBS -l ncpus=1
#PBS -j oe
#PBS -q normal
#PBS -l wd
#PBS -l other=gdata1




module load cdo
module load nco

ds="CMIP6"

#Direcotry for storing processed datasets
DIR="/g/data1/w35/amu561/CMIP6_drought/"

IN_DIR=$DIR"/${ds}_Data/Processed_${ds}_data"


OUT_DIR="${DIR}/Mean_and_variability_change/"
mkdir -p $OUT_DIR

#Find experiments
experiments=(historical ssp245 ssp585)


### Loop through experiments ###

for E in "${experiments[@]}" 
do
  
  echo "Processing experiment: ${E}"

  #Variable
  V="pr"

  #Find variables (var_table/var combination)
  models=`ls $IN_DIR/$E/$V/`   #`find ${V} -maxdepth 1 -mindepth 1 ! -path . -type d`

  for M in $models
  do

    #Find ensemble name
    ensembles=`ls $IN_DIR/${E}/${V}/${M}`


    ### Loop through ensemble members ###

    for ens in $ensembles
    do
      
      out_temp=${OUT_DIR}/${ds}/${E}/${V}/${M}/
      mkdir -p $out_temp
      
      #Find file
      ifile=`find $IN_DIR/${E}/${V}/${M}/${ens}/*setgrid.nc`


      #Set time period
      if [[ $E =~ "historical" ]]; then
        
        start_yr=1950
        end_yr=2014

      else    
        
        start_yr_early=2015
        end_yr_early=2050
        
        start_yr_late=2051
        end_yr_late=2100
        
        
      fi
      
        
      ### Historical ###
      
      if [[ $E =~ "historical" ]]; then

        #Select years and set missing value
        temp_file=$out_temp"/temp_file.nc"
        
        cdo -L selyear,$start_yr/$end_yr -setmissval,-9999 \
        -setrtomiss,-1000000000000,0 $ifile $temp_file

        #Calculate mean
        cdo -L -b F64 timmean $temp_file $out_temp"/mean_${V}_${M}_${start_yr}_"${end_yr}_"${ens}.nc"

        #Calculate variability (standard deviation)
        cdo -L -b F64 timstd -detrend $temp_file $out_temp"/stdev_${V}_${M}_${start_yr}_${end_yr}_${ens}.nc"
        
        #Remove temp file
        rm $temp_file
      
      
      
      ### Future periods ###
      
      else 
        
        #Set missing value
        temp_file=$out_temp"/temp_file.nc"
        
        cdo -L -setmissval,-9999 -setrtomiss,-1000000000000,0 $ifile $temp_file


        ### First period (2015-2050) ###
        
        #Calculate mean
        cdo -L -b F64 timmean -selyear,$start_yr_early/$end_yr_early $temp_file \
            $out_temp"/mean_${V}_${M}_${start_yr_early}_${end_yr_early}_${ens}.nc"

        #Calculate variability (standard deviation)
        cdo -L -b F64 timstd -detrend -selyear,$start_yr_early/$end_yr_early \
            $temp_file $out_temp"/stdev_${V}_${M}_${start_yr_early}_${end_yr_early}_${ens}.nc"
        
        
        ### Second period (2051-2100) ###
        
        #Calculate mean
        cdo -L -b F64 timmean -selyear,$start_yr_late/$end_yr_late $temp_file \
            $out_temp"/mean_${V}_${M}_${start_yr_late}_${end_yr_late}_${ens}.nc"

        #Calculate variability (standard deviation)
        cdo -L -b F64 timstd -detrend -selyear,$start_yr_late/$end_yr_late \
            $temp_file $out_temp"/stdev_${V}_${M}_${start_yr_late}_${end_yr_late}_${ens}.nc"
        

        
        
        #Remove temp file
        rm $temp_file
        
      fi #experiment
      
      
    done #ensembles
  done #models
done #experiments





