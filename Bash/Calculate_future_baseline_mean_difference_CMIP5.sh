#!/bin/bash

#PBS -P dt6
#PBS -l walltime=10:00:00
#PBS -l mem=5000MB
#PBS -l ncpus=1
#PBS -j oe
#PBS -q normal
#PBS -l wd
#PBS -l other=gdata1
#PBS -l jobfs=1GB

module load cdo
module load nco

ds="CMIP5"

#Direcotry for storing processed datasets
DIR="/g/data1/w35/amu561/CMIP6_drought/"

IN_DIR=$DIR"/${ds}_Data/Processed_${ds}_data/"


OUT_DIR="${DIR}/Mean_and_percentile/"
mkdir -p $OUT_DIR

#Find experiments
experiments=(historical rcp45 rcp85)


### Loop through experiments ###

for E in "${experiments[@]}" 
do

  
  
  echo "Processing experiment: ${E}"

  #Variable
  V="pr"

  #Find variables (var_table/var combination)
  models=`ls $IN_DIR/${E}/${V}/`   #`find ${V} -maxdepth 1 -mindepth 1 ! -path . -type d`

  for M in $models
  do

    #Find ensemble name
    ensembles=`ls $IN_DIR/${E}/$V/$M`


    ### Loop through ensemble members ###

    for ens in $ensembles
    do
      
      out_temp=${OUT_DIR}/${ds}/${E}/${V}/${M}/
      mkdir -p $out_temp
      
      ifile=`find $IN_DIR/${E}/${V}/${M}/${ens}/*setgrid.nc`


      if [[ $E =~ "historical" ]]; then
        
        #Select time period
        temp_file=$out_temp"/temp_file.nc"
        
        cdo selyear,1955/2005 $ifile $temp_file

        #Calculate seasonal means
        cdo -L yseasmean $ifile $out_temp"/seasonal_means_1955_2005_${ens}.nc"

        cdo -L yseaspctl,15 $ifile -yseasmin $ifile -yseasmax $ifile $out_temp"/seasonal_15th_percentile_1955_2005_${ens}.nc"

        rm $temp_file

      
      else
        
        ### First time period ###
        
        #Select time period
        temp_file=$out_temp"/temp_file.nc"
        
        cdo selyear,2015/2050 $ifile $temp_file

        #Calculate seasonal means
        cdo -L yseasmean $ifile $out_temp"/seasonal_means_2015_2050_${ens}.nc"

        cdo -L yseaspctl,15 $ifile -yseasmin $ifile -yseasmax $ifile $out_temp"/seasonal_15th_percentile_2015_2050_${ens}.nc"

        rm $temp_file


        ### Second time period ###
        
        #Select time period
        temp_file=$out_temp"/temp_file.nc"
        
        cdo selyear,2051/2099 $ifile $temp_file

        #Calculate seasonal means
        cdo -L yseasmean $ifile $out_temp"/seasonal_means_2051_2099_${ens}.nc"

        cdo -L yseaspctl,15 $ifile -yseasmin $ifile -yseasmax $ifile $out_temp"/seasonal_15th_percentile_2051_2099_${ens}.nc"

        rm $temp_file

      
      
      
      fi





    done #ensembles
  done #models
done #experiments





