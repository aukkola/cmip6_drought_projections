#!/bin/bash

#PBS -P dt6
#PBS -l walltime=2:00:00
#PBS -l mem=5000MB
#PBS -l ncpus=1
#PBS -j oe
#PBS -q normal
#PBS -l wd
#PBS -l other=gdata1

module load cdo
module load nco

ds="CMIP5"

#Direcotry for storing processed datasets
DIR="/g/data1/w35/amu561/CMIP6_drought/"

IN_DIR=$DIR"/Drought_metrics_${ds}/"


OUT_DIR="${DIR}/Drought_metric_testing/"
mkdir -p $OUT_DIR

#Find experiments
experiments=(historical rcp45 rcp85)

perc="Perc_15"
baseline="Baseline_1950_2014"
scale="Scale_3"

### Loop through experiments ###

for E in "${experiments[@]}" 
do

  
  
  echo "Processing experiment: ${E}"

  #Variable
  V="pr"

  #Find variables (var_table/var combination)
  models=`ls $IN_DIR/$E/$V/$perc/$baseline/$scale`   #`find ${V} -maxdepth 1 -mindepth 1 ! -path . -type d`

  for M in $models
  do

    #Find ensemble name
    ensembles=`ls $IN_DIR/${E}/${V}/$perc/$baseline/$scale/${M}`


    ### Loop through ensemble members ###

    for ens in $ensembles
    do
      
      out_temp=${OUT_DIR}/${ds}/${E}/${V}/${M}/
      mkdir -p $out_temp
      
      #Find file
      ifile="$IN_DIR/${E}/${V}/$perc/$baseline/$scale/${M}/$ens" # `find $IN_DIR/${E}/${V}/${M}/${ens}/*setgrid.nc`


      #Set time period
      if [[ $E =~ "historical" ]]; then
        
        start_yr=1951
        end_yr=2000

      else    
        
        start_yr=2041
        end_yr=2090
      fi
      
        
      ##################
      ### P15 and P0 ###
      ##################
      
      temp_file=$out_temp"/temp_file.nc"
      
      cdo -L selyear,$start_yr/$end_yr -setrtomiss,-1000000000000,0 -setmissval,-9999 \
      -selname,tseries $ifile $temp_file

      #Calculate monthly minimum
      cdo -L -b F64 ymonmin $temp_file $out_temp"/monthly_minimum_${start_yr}_${end_yr}_${ens}.nc"

      #Calculate monthly 15th percentile
      cdo -L -b F64 ymonpctl,15 $temp_file -ymonmin $temp_file -ymonmax $temp_file \
      $out_temp"/monthly_perc15_"${start_yr}"_"${end_yr}"_"${ens}

      rm $temp_file


        
      ###########################################
      ### Mean drought duration and intensity ###
      ###########################################

      ### Duration ###
      
      temp_file=$out_temp"/temp_file.nc"
      
      cdo -L selyear,$start_yr/$end_yr -setmissval,-9999 \
      -setrtomiss,-1000000000000,0 -selname,duration $ifile $temp_file

      cdo -L -b F64 timmean $temp_file $out_temp"/mean_duration_"${start_yr}"_"${end_yr}"_"${ens}

      rm $temp_file
      
      
      ### Intensity ###
      
      temp_file=$out_temp"/temp_file.nc"
      
      cdo -L selyear,$start_yr/$end_yr -setmissval,-9999 -setrtomiss,-1000000000000,0 \
      -selname,intensity $ifile $temp_file

      cdo -L -b F64 timmean $temp_file $out_temp"/mean_intensity_"${start_yr}"_"${end_yr}"_"${ens}

      rm $temp_file
      
      
      ### Frequency ###

      #Get time period and variable (also set missing value, not done correctly
      #in python generated files)
      temp_file=$out_temp"/temp_file.nc"

      cdo -L selyear,$start_yr/$end_yr -setmissval,-9999 -setrtomiss,-1000000000000,0 \
      -selname,duration $ifile $temp_file


      #Calculate number of missing values
      temp_file_miss=$out_temp"/temp_file_miss.nc"

      ncap2 -s 'frequency=duration.missing().ttl($time)' $temp_file $temp_file_miss
      
  
      #Convert this to number of non-missing values
      ntime=`cdo ntime $temp_file_miss`
      cdo -L expr,frequency=${ntime}-frequency -selname,frequency $temp_file_miss \
             $out_temp"/frequency_"${start_yr}"_"${end_yr}"_"${ens}

      rm $temp_file $temp_file_miss

    done #ensembles
  done #models
done #experiments





