#!/bin/bash

#PBS -P dt6
#PBS -l walltime=2:00:00
#PBS -l mem=15GB
#PBS -l ncpus=1
#PBS -j oe
#PBS -q normal
#PBS -l wd
#PBS -l other=gdata1




module load cdo
module load nco

ds="Observed"

#Direcotry for storing processed datasets
DIR="/g/data1/w35/amu561/CMIP6_drought/"

IN_DIR=$DIR"/${ds}_Data/"


OUT_DIR="${DIR}/Mean_and_variability_change/"
mkdir -p $OUT_DIR

#Find experiments


### Loop through experiments ###


#Variable
V="pr"

#Find variables (var_table/var combination)
models=`ls $IN_DIR`   #`find ${V} -maxdepth 1 -mindepth 1 ! -path . -type d`

for M in $models
do

  
  out_temp=${OUT_DIR}/${ds}/"observed"/${V}/${M}/
  mkdir -p $out_temp
  
  #Find file
  ifile=`find $IN_DIR/${M}/*.nc`


  #Set time period
    
  start_yr=1950
  end_yr=2014


  #Select years and set missing value
  temp_file=$out_temp"/temp_file.nc"
  
  cdo -L selyear,$start_yr/$end_yr -setmissval,-9999 \
  -setrtomiss,-1000000000000,0 $ifile $temp_file

  #Calculate mean
  cdo -L -b F64 timmean $temp_file $out_temp"/mean_${V}_${M}_${start_yr}_${end_yr}.nc"

  #Calculate variability (standard deviation)
  cdo -L -b F64 timstd -detrend $temp_file $out_temp"/stdev_${V}_${M}_${start_yr}_${end_yr}.nc"
  
  #Remove temp file
  rm $temp_file


done #models





