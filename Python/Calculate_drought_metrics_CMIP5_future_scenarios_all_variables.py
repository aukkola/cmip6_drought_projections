# -*- coding: utf-8 -*-
"""
Created on Wed Jun 22 14:34:05 2016

@author: annaukkola

"""

from netCDF4 import Dataset,num2date# to work with NetCDF files
import numpy as np
import glob
import sys 
import os
import datetime


### Set paths ###

root_path = "/g/data1/w35/amu561/CMIP6_drought/"
lib_path  = root_path + '/scripts/Drought_metrics/functions' # '/drought_metric_scripts/Drought_metrics/functions'


sys.path.append(os.path.abspath(lib_path))
from drought_metrics import *

   
   
### Set variable ###

var_name=["pr"]#, "mrro", "mrros"]     #Variable name in netcdf file
var_path=var_name #This one is only used to create path/file names


#######################
### Set experiments ###
#######################

experiment=['rcp45', 'rcp85']  #CMIP5 experiments
#experiment=['historical']  #CMIP5 experiments

#################
### Set years ###
#################


#Set baseline
baseline = [1950, 2014]  #N.B. longer than cmip5 historical, extend with RCP



#Select if want indices returned as a time series or shorter vector
#If True, will return drought indices as time series (with NA for non-drought months)
#useful for calculating trends etc. in metrics
#If False, collapses indices into a short vector 
#reduces data size and useful if looking at the mean of drought indices
return_all_tsteps=True

#Only calculate metrics for first ensemble member (r1i1p1 if exists)?
use_first_ensemble=False



############################
#### Set drought limits ####
############################

#Set percentile for drought threshold
perc=15

#Set scale if want to use running means to determine drought (like SPI scale)
#Use 1 if don't want to use this
scale=3

#Use threshold determined separately for each month?
#If set to false, determines one threshold from all data.
#Set to false if using annual data
monthly=True


#Use observations or another model file for calculating threshold?
#Uses this file to calculate baseline for drought metrics if true
#(currently set to use historical simulation, see "obs_file" below)
obs_ref = True
obs_var = var_name   #ET_mean, ET_min, ET_max



##################
### Load files ###
##################

data_path = root_path + '/CMIP5_Data/Processed_CMIP5_data/'

#Loop through variables
for v in range(len(var_name)):
    
    #Progress
    print("#--------------- Variable " + str(v+1) + "/" + str(len(var_name)))
    
    
    #Loop through experiments
    for k in range(len(experiment)):

        #List all model names
        models = os.listdir(data_path + experiment[k] + "/" + var_name[v] + "/")


        #Reference data for calculating threshold
        for m in range(len(models)):

            ensembles=os.listdir(data_path + experiment[k] + "/" + var_name[v] + 
                                 "/" + models[m])
            
            
            #If only processing first ensemble member
            if use_first_ensemble:
                #Take r1i1p1 if it exists
                if "r1i1p1" in ensembles:
                    ensembles = [ensembles[ensembles.index("r1i1p1")]]
                #Else take first available
                else:
                    #sort in case python doesn't return in alphabetical order
                    ensembles.sort()
                    ensembles = [ensembles[0]]
            
            
            #Loop through ensembles
            for e in range(len(ensembles)):
            
                        
                #Print progress
                print('Experiment: ' + str(k+1) + '/' + str(len(experiment)) + ', model: ' + 
                      str(m+1) + "/" + str(len(models)) + ', ensemble:' + str(e+1) + '/' + 
                      str(len(ensembles)))


                #If using obs as reference (set to ET data, fix later if needed...)
                if obs_ref:
                    obs_file = glob.glob(data_path + '/historical/' + var_name[v] + 
                                         "/" + models[m] + "/" + ensembles[e] +
                                         "/*setgrid.nc")
                                         
                    #Skip if didn't find files
                    if len(obs_file) ==0 :
                        continue
    
            
                ### Find CMIP5 files ###
                files = glob.glob(data_path + experiment[k] + "/" + var_name[v] + 
                                  "/" + models[m] + "/" + ensembles[e] + 
                                  "/*setgrid.nc")
                
                
                
                #Skip if didn't find files
                if len(files) ==0 :
                    continue
    
    
                #################
                ### Load data ###
                #################
                
                
                #Model data
                fh = Dataset(files[0], mode='r')
                all_data = fh.variables[var_name[v]][:] #[yr_ind]
                data     = all_data.data
                mask     = all_data.mask
                fh_time  = fh.variables["time"]
                
                
                #Get lon and lat (name varies by CMIP5 model)
                try:
                    lat = fh.variables['latitude'][:]
                    lon = fh.variables['longitude'][:]
                except:
                    try:
                        lat = fh.variables['lat'][:] #northing
                        lon = fh.variables['lon'][:] #easting
                    except:
                        lat = fh.variables['northing'][:] #northing
                        lon = fh.variables['easting'][:] #easting
                    
             
                #Get dataset dates
                fh_dates = num2date(fh_time[:], fh_time.units, calendar=fh_time.calendar)
                fh_years = np.array([y.year for y in fh_dates])

                #Extract time vector, need to modify this if extending with RCP8.5
                fh_tstamps = fh_time[:]
 
                #Correction for Hadley Centre models, time series start from
                #Dec, leading to incomplete first year. Remove this
                #(check if time series length is divisible by 12)
                if (fh_years[0] != fh_years[1]):
                    
                    start = np.where(fh_years == fh_years[0]+1)[0][0]
                    end   = len(fh_years)        
                    
                    data = data[start:(end+1),:,:]
                    mask = mask[start:(end+1),:,:]
                    fh_years = fh_years[start:(end+1)]
                    fh_tstamps = fh_tstamps[start:(end+1)]

                #Ensemble r1i1p1 for HadGEM2-ES rcp8.5 has an extra time step,
                #remove this
                if ensembles[e]=="r1i1p1" and models[m]=="HadGEM2-ES" and data.shape[0]%12 != 0 :
                    rm_ind = np.where(fh_years == 2099)[0][-1]

                    data = np.delete(data, rm_ind, axis=0)
                    mask = np.delete(mask, rm_ind, axis=0)
                    fh_years   = np.delete(fh_years, rm_ind)
                    fh_tstamps = np.delete(fh_tstamps, rm_ind)



                #fh.close()
                
                #Mask                            
                miss_val = -99999.0
                data[mask==True] = miss_val

                miss_val = -99999.0
                data[mask==True] = miss_val
            
                #Read reference data used to calculate threshold
                if obs_ref:
                    obs_fh      = Dataset(obs_file[0], mode='r')
                    control_ref = obs_fh.variables[obs_var[v]][:].data
                    obs_time    = obs_fh.variables["time"]
                    
                    #Get lon and lat (name varies by CMIP5 model)
                    try:
                        lat_ctrl = fh.variables['latitude'][:]
                        lon_ctrl = fh.variables['longitude'][:]
                    except:
                        lat_ctrl = fh.variables['lat'][:]
                        lon_ctrl = fh.variables['lon'][:]
                     

                    print("Using an OBS/model ref to calculate baseline")
                
                
                    #Get dataset dates
                    obs_dates = num2date(obs_time[:], obs_time.units,
                                         calendar=obs_time.calendar)
                    obs_years = np.array([y.year for y in obs_dates])
    
    
                    #Correction for Hadley Centre models, time series start from
                    #Dec, leading to incomplete first year. Remove this
                    #(check if time series length is divisible by 12)
                    if (obs_years[0] != obs_years[1]):
                        
                        start = np.where(obs_years == obs_years[0]+1)[0][0]
                        end   = len(obs_years)        
                        
                        control_ref = control_ref[start:(end+1),:,:]
                        obs_years    = obs_years[start:(end+1)]

                        
                    #Extend obs historical tseries with RCP if needed        
                    if baseline[1] > obs_years[-1]:
                        
                        #Hadley Centre correction (data ends in November)
                        if len(np.where(obs_years == np.max(obs_years))[0]) != 12 :
                            
                            #My god, different ensemble members also do different things
                            #r1i1p1 is missing Dec 2005 altogether, repeat November
                            if fh_years[0] > np.max(obs_years) :
                                obs_years   = np.append(obs_years, 2005)
                                control_ref = np.concatenate([control_ref, control_ref[(control_ref.shape[0]-1):control_ref.shape[0], :, :]])
                            
                                start_ind = np.where(fh_years == obs_years[-1] + 1)[0][0]
                                
                            else :
                                start_ind = np.where(fh_years == obs_years[-1])[0][-1]
                        
                        else:
                            start_ind = np.where(fh_years == obs_years[-1] + 1)[0][0]
                        
                        
                        end_ind = np.where(fh_years == baseline[1])[0][-1]
                        
                        #Extend obs data
                        control_ref = np.concatenate([control_ref, data[start_ind:(end_ind+1),:,:]]) #add 1, stupid python indexing
                
                        #Append new years
                        obs_years = np.append(obs_years, fh_years[start_ind:(end_ind+1)])
                
                
                else:
                    control_ref = data        
                    
                           
                ###################################################
                ### Create output file name and check if exists ###
                ###################################################
                
                #Creating this here so can check if it already exists,
                #and skip to speed up processing
                        
                #Creat output path
                out_path = (root_path + '/Drought_metrics_CMIP5/' + experiment[k] + "/" + 
                            var_path[v]) # + '/Obs_' + str(obs_ref) )

                #Add percentile
                out_path = (out_path + '/Perc_' + str(perc) + '/Baseline_' + 
                            str(baseline[0]) + "_" + str(baseline[1]) +
                            "/Scale_" + str(scale) + "/" + models[m] + "/")
            
                
                #Create output directory if doesn't exist
                if not os.path.exists(out_path):    
                    os.makedirs(out_path)
                
                #Create output file name
                out_file = (out_path + '/' + models[m] + "_" + ensembles[e] + 
                            '_drought_metrics_perc_' + str(perc))

                out_file = (out_file + "_" + experiment[k] + '_' + str(fh_years[0]) + 
                            '_' + str(fh_years[-1]) + '.nc')
                        
                
                #Check if exists and Skip
                if os.path.isfile(out_file):
                    print("Skipping " + experiment[k] + ", " + models[m] + ", " + 
                          ensembles[e] + ", already exists")
                    continue
                


                    
                #############################
                ### Find baseline indices ###
                #############################
                
                #Get dates
                ref_years = fh_years
                if obs_ref:
                    ref_years = obs_years
                    
                #Find indices corresponding to start of first year,
                #and end of end year, defined by baseline
                
                #Add an exception for CESM1-WACCM, only reports 1955-2005
                #for some historical runs
                if (models[m] == "CESM1-WACCM" and ref_years[0] > baseline[0]):
                    #Create indices for baseline period
                    subset = range(0, np.where(ref_years == baseline[1])[0][-1] + 1) #Stupid python indexing
                                   
                else:    
                    #Create indices for baseline period
                    subset = range(np.where(ref_years == baseline[0])[0][0],
                                   np.where(ref_years == baseline[1])[0][-1] + 1) #Stupid python indexing
                                    

                        
                #Not sure why but python reads some files upside down
                #Flip latitude if reads map upside down so model matches Obs data
                if obs_ref:
                    #If model data upside down
                    if (lat[0] < 0 and lat_ctrl[0] > 0):
                        print("Flipping MODEL data, experiment: ", experiment[k], 
                              " model:", models[m])
                        data = data[:,::-1,:]
                        #replace lat with lat_ctrl (otherwise written to file the wrong way round)
                        lat=lat_ctrl
                    #If REF data upside down
                    elif (lat[0] > 0 and lat_ctrl[0] < 0):
                        print("Flipping OBS ref data, experiment: ", experiment[k], 
                              " model:", models[m])
                        control_ref = control_ref[:,::-1,:]


            
                ### Initialise output arrays ###
                
                #Expect to have approx. percentile amount of months as drought (e.g. 10% of data
                #when percentile is set to 10. To be on the safe side, determine array sizes
                #as double that size) if not writing out as a full time series
                
                if return_all_tsteps:
                    save_len = len(data)
                else:
                    save_len = int(len(data)*(perc/100)*2)
                
                duration      = np.zeros((save_len, len(lat), len(lon))) + miss_val # * np.nan
                rel_intensity = np.zeros((save_len, len(lat), len(lon))) + miss_val # * np.nan
                intensity     = np.zeros((save_len, len(lat), len(lon))) + miss_val # * np.nan
                timing        = np.zeros((save_len, len(lat), len(lon))) + miss_val # * np.nan    
                tseries       = np.zeros((save_len, len(lat), len(lon))) + miss_val # * np.nan    

                if monthly:
                    threshold    = np.zeros((12, len(lat), len(lon))) + miss_val # * np.nan
                else:
                    threshold    = np.zeros((len(lat), len(lon))) + miss_val # * np.nan
            
            
                #########################
                ### Calculate metrics ###
                #########################
                
                
                #Loop through grid cells
                for i in range(len(lat)):
                            
                    for j in range(len(lon)):
                    
                         #Calculate metrics if cell not missing
                         if any(~mask[:,i,j]):  
                         
                             #Calculate metrics
                             metric = drought_metrics(mod_vec=data[:,i,j], lib_path=lib_path, perc=perc, 
                                                      monthly=monthly, obs_vec=control_ref[:,i,j],
                                                      return_all_tsteps=return_all_tsteps, scale=scale,
                                                      add_metrics=(['timing', 'rel_intensity', 'intensity', 'threshold']),
                                                      subset=subset)
                        
                             ### Write metrics to variables ###
                             duration[range(np.size(metric['duration'])),i,j]   = metric['duration']  #total drought duration (months)
            
                             rel_intensity[range(np.size(metric['rel_intensity'])),i,j] = metric['rel_intensity'] #average magnitude
                            
                             intensity[range(np.size(metric['intensity'])),i,j] = metric['intensity'] #average intensity
                    
                             timing[range(np.size(metric['timing'])),i,j]       = metric['timing']    #drought timing (month index)
                             
                             tseries[range(np.size(metric['tseries'])),i,j]     = metric['tseries']    #drought timing (month index)


           
                             if monthly:
                                 threshold[:,i,j] = metric['threshold'][0:12]    #drought timing (month index)
                             else:
                                 threshold[i,j]   = metric['threshold'][0]
            
            
                ##############################
                ### Write result to NetCDF ###
                ##############################
                                
                
                # Open a new netCDF file for writing
                ncfile = Dataset(out_file,'w', format="NETCDF4_CLASSIC") 
                
                # Create the output data
                # Create the x, y and time dimensions
                ncfile.createDimension('lat', lat.shape[0])
                ncfile.createDimension('lon', lon.shape[0])
                ncfile.createDimension('time', save_len)
                    
                if monthly:
                    ncfile.createDimension('month', 12)
            
            
            
                # Create dimension variables
            
                longitude = ncfile.createVariable("lon",  'f8', ('lon',))
                latitude  = ncfile.createVariable("lat",  'f8', ('lat',))
                time      = ncfile.createVariable("time", 'i4', ('time',))
            
                if monthly:
                    month = ncfile.createVariable("month", 'i4', ('month',))
            
                #Create data variables
                data_dur  = ncfile.createVariable('duration', 'i4',('time','lat','lon'), fill_value=miss_val)
                data_mag  = ncfile.createVariable('rel_intensity','f8',('time','lat','lon'), fill_value=miss_val)
                data_int  = ncfile.createVariable('intensity','f8',('time','lat','lon'), fill_value=miss_val)
                data_tim  = ncfile.createVariable('timing',   'i4',('time','lat','lon'), fill_value=miss_val)
                data_ts   = ncfile.createVariable('tseries',   'i4',('time','lat','lon'), fill_value=miss_val)

                #Create data variable for threshold
                if monthly:
                    data_thr = ncfile.createVariable('threshold', 'f8',('month','lat','lon'), fill_value=miss_val)
                else:
                    data_thr = ncfile.createVariable('threshold', 'f8',('lat','lon'), fill_value=miss_val)
            
            
                #Set variable attributes
                longitude.units = 'degrees_east'
                latitude.units  = 'degrees_north'
                time.units      = fh_time.units
                
                time.calendar   = fh_time.calendar


                data_dur.long_name = 'drought event duration (no. months)'
                data_mag.long_name = 'drought event relative intensity (%)'
                data_int.long_name = 'drought event intensity (mm)'
                data_tim.long_name = 'drought event timing (month index)'
                data_thr.long_name = 'drought threshold (mm)'
                data_ts.long_name  = 'original time series'

                if monthly:
                    month[:] = range(1,12+1)
             
                # Write data to dimension variables
                longitude[:]=lon
                latitude[:] =lat
            
                #If saving all time steps
                if return_all_tsteps:
                    time[:] = fh_tstamps[:]
                else:
                    time[:] = range(1, save_len+1)
                       
                if monthly:
                    month[:] = range(1,12+1)
             
                #Write data to data variables
                data_dur[:,:,:] = duration    
                data_mag[:,:,:] = rel_intensity
                data_int[:,:,:] = intensity
                data_tim[:,:,:] = timing
                data_ts[:,:,:]  = tseries

                if monthly:    
                    data_thr[:,:,:] = threshold
                else:
                    data_thr[:,:] = threshold
                
                
                # Close the file
                ncfile.close()










