# -*- coding: utf-8 -*-
"""
Created on Wed Jun 22 14:34:05 2016

@author: annaukkola

"""


from netCDF4 import Dataset,num2date # to work with NetCDF files
import numpy as np
import glob
import sys 
import os
import datetime


### Set paths ###

root_path = "/srv/ccrc/data04/z3509830/CMIP6_drought/"
lib_path  = root_path + '/drought_metric_scripts/Drought_metrics/functions'


sys.path.append(os.path.abspath(lib_path))
from drought_metrics import *

   
   

#######################
### Set experiments ###
#######################

datasets=['GRUN']  #CMIP5 experiments
#experiment=['historical']  #CMIP5 experiments


### Set variable names ###

var_name=["Runoff"]    #Variable name in netcdf file
var_path="mrro" #This one is only used to create path/file names


#################
### Set years ###
#################

#Set baseline
baseline = [1950, 2014]

#Set dataset years to use 
start_yr= [1902]
end_yr  = [2014]


#Select if want indices returned as a time series or shorter vector
#If True, will return drought indices as time series (with NA for non-drought months)
#useful for calculating trends etc. in metrics
#If False, collapses indices into a short vector 
#reduces data size and useful if looking at the mean of drought indices
return_all_tsteps=True

############################
#### Set drought limits ####
############################

#Set percentile for drought threshold
perc=15

#Set scale if want to use running means to determine drought (like SPI scale)
#Use 1 if don't want to use this
scale=3

#Use threshold determined separately for each month?
#If set to false, determines one threshold from all data.
#Set to false if using annual data
monthly=True


#Use observations or another model file for calculating threshold?
#Uses this file to calculate baseline for drought metrics if true
#(currently set to use historical simulation, see "obs_file" below)
obs_ref = False
obs_var = var_name   #ET_mean, ET_min, ET_max



##################
### Load files ###
##################

#Loop through experiments
for k in range(len(datasets)):


            ### Find CMIP5 files ###
            files = glob.glob(root_path + '/../Obs_runoff_products/' +
                              datasets[k] + "/Data_" + str(start_yr[k]) + 
                              "_" + str(end_yr[k]) + "/*.nc")
            
            
            
            ###################################################
            ### Create output file name and check if exists ###
            ###################################################
            
            #Creating this here so can check if it already exists,
            #and skip to speed up processing
                    
            #Creat output path
            out_path = (root_path + '/Drought_metrics/observed/' + var_path)

            #Add percentile
            out_path = (out_path + '/Perc_' + str(perc) + '/Baseline_' + 
                        str(baseline[0]) + "_" + str(baseline[1]) +
                        "/Scale_" + str(scale) + '/' + datasets[k] + "/")
        
            
            #Create output directory if doesn't exist
            if not os.path.exists(out_path):    
                os.makedirs(out_path)
            
            #Create output file name
            out_file = (out_path + '/' + datasets[k] + '_drought_metrics_perc_' + 
                        str(perc) + "_" + str(start_yr[k]) + '_' + str(end_yr[k]) + '.nc')
                    
            
            #Check if exists and Skip
            if os.path.isfile(out_file):
                continue
            



            #################
            ### Load data ###
            #################
            
            #Model data
            fh = Dataset(files[0], mode='r')
            all_data = fh.variables[var_name[k]][:] #[yr_ind]
            data     = all_data#.data
            fh_time     = fh.variables["time"]

                
            
            #Get lon and lat (name varies by CMIP5 model)
            try:
                lat = fh.variables['latitude'][:]
                lon = fh.variables['longitude'][:]
            except:
                lat = fh.variables['lat'][:] #northing
                lon = fh.variables['lon'][:] #easting
         
         
            #Get dataset dates
            fh_dates = num2date(fh_time[:], fh_time.units, calendar=fh_time.calendar)
            fh_years = np.array([y.year for y in fh_dates])

            #fh.close()
            
            #Mask
            #If mask one value, make it an array that has same dimensions
            #as data (I think this happens because oceans haven't been
            #masked out)
            #Some files don't have mask at all, hmmm
            try:
                mask = all_data.mask                    
            except: 
                mask = np.zeros(data.shape, dtype='bool')

            if mask.shape == ():
                mask = np.zeros(data.shape, dtype='bool')
                            
                            
            miss_val = -99999.0
            data[mask==True] = miss_val
        
            #Not using obs_ref here so set to data
            control_ref = data        
                
    
            #Flip latitude as reads map upside down
            #Should implement this if using pet or temp limits... Fix later
        
            #Not sure why but python reads some files upside down
            #Flip latitude if reads map upside down so model matches SPI data
            if obs_ref:
                #If model data upside down
                if (lat[0] < 0 and lat_ctrl[0] > 0):
                    print("Flipping MODEL data, experiment: ", experiment[k], 
                          " model:", models[m])
                    data = data[:,::-1,:]
                    #replace lat with lat_ctrl (otherwise written to file the wrong way round)
                    lat=lat_ctrl
                #If REF data upside down
                elif (lat[0] > 0 and lat_ctrl[0] < 0):
                    print("Flipping OBS ref data, experiment: ", experiment[k], 
                          " model:", models[m])
                    control_ref = control_ref[:,::-1,:]


            #############################
            ### Find baseline indices ###
            #############################
            
            #Get dates
            ref_years = fh_years
            if obs_ref:
                ref_years = obs_years
                
            #Find indices corresponding to start of first year,
            #and end of end year, defined by baseline
            
           
            #Create indices for baseline period
            subset = range(np.where(ref_years == baseline[0])[0][0],
                           np.where(ref_years == baseline[1])[0][-1] + 1) #Stupid python indexing


            ################################
            ### Initialise output arrays ###
            ################################

            #Expect to have approx. percentile amount of months as drought (e.g. 10% of data
            #when percentile is set to 10. To be on the safe side, determine array sizes
            #as double that size) if not writing out as a full time series
            
            if return_all_tsteps:
                save_len = len(data)
            else:
                save_len = int(len(data)*(perc/100)*2)
            
            duration      = np.zeros((save_len, len(lat), len(lon))) + miss_val # * np.nan
            rel_intensity = np.zeros((save_len, len(lat), len(lon))) + miss_val # * np.nan
            intensity     = np.zeros((save_len, len(lat), len(lon))) + miss_val # * np.nan
            timing        = np.zeros((save_len, len(lat), len(lon))) + miss_val # * np.nan    
            
            if monthly:
                threshold    = np.zeros((12, len(lat), len(lon))) + miss_val # * np.nan
            else:
                threshold    = np.zeros((len(lat), len(lon))) + miss_val # * np.nan
        
        
            #########################
            ### Calculate metrics ###
            #########################
            
            #Print progress
            print('Dataset: ', (k+1))
                  
            
            #Loop through grid cells
            for i in range(len(lat)):
                        
                for j in range(len(lon)):
                
                     #Calculate metrics if cell not missing
                     if any(~mask[:,i,j]):  
                     
                         #Calculate metrics
                         metric = drought_metrics(mod_vec=data[:,i,j], lib_path=lib_path, perc=perc, 
                                                  monthly=monthly, obs_vec=control_ref[:,i,j],
                                                  return_all_tsteps=return_all_tsteps, scale=scale,
                                                  add_metrics=(['timing', 'rel_intensity', 'intensity', 'threshold']),
                                                  subset=subset)
                    
                         ### Write metrics to variables ###
                         duration[range(np.size(metric['duration'])),i,j]   = metric['duration']  #total drought duration (months)
        
                         rel_intensity[range(np.size(metric['rel_intensity'])),i,j] = metric['rel_intensity'] #average magnitude
                        
                         intensity[range(np.size(metric['intensity'])),i,j] = metric['intensity'] #average intensity
                
                         timing[range(np.size(metric['timing'])),i,j]       = metric['timing']    #drought timing (month index)
     
       
                         if monthly:
                             threshold[:,i,j] = metric['threshold'][0:12]    #drought timing (month index)
                         else:
                             threshold[i,j]   = metric['threshold']
        
        
            ##############################
            ### Write result to NetCDF ###
            ##############################
                            
            
            # Open a new netCDF file for writing
            ncfile = Dataset(out_file,'w', format="NETCDF4_CLASSIC") 
            
            # Create the output data
            # Create the x, y and time dimensions
            ncfile.createDimension('lat', lat.shape[0])
            ncfile.createDimension('lon', lon.shape[0])
            ncfile.createDimension('time', save_len)
                
            if monthly:
                ncfile.createDimension('month', 12)
        
        
        
            # Create dimension variables
        
            longitude = ncfile.createVariable("lon",  'f8', ('lon',))
            latitude  = ncfile.createVariable("lat",  'f8', ('lat',))
            time      = ncfile.createVariable("time", 'i4', ('time',))
        
            if monthly:
                month = ncfile.createVariable("month", 'i4', ('month',))
        
            #Create data variables
            data_dur  = ncfile.createVariable('duration', 'i4',('time','lat','lon'), fill_value=miss_val)
            data_mag  = ncfile.createVariable('rel_intensity','f8',('time','lat','lon'), fill_value=miss_val)
            data_int  = ncfile.createVariable('intensity','f8',('time','lat','lon'), fill_value=miss_val)
            data_tim  = ncfile.createVariable('timing',   'i4',('time','lat','lon'), fill_value=miss_val)
            
            #Create data variable for threshold
            if monthly:
                data_thr = ncfile.createVariable('threshold', 'f8',('month','lat','lon'), fill_value=miss_val)
            else:
                data_thr = ncfile.createVariable('threshold', 'f8',('lat','lon'), fill_value=miss_val)
        
        
            #Set variable attributes
            longitude.units = 'degrees_east'
            latitude.units  = 'degrees_north'
            time.units      = fh_time.units
            
            time.calendar   = fh_time.calendar

            
            data_dur.long_name= 'drought event duration (no. months)'
            data_mag.long_name= 'drought event relative intensity (%)'
            data_int.long_name= 'drought event intensity (mm)'
            data_tim.long_name= 'drought event timing (month index)'
            data_thr.long_name= 'drought threshold (mm)'
           
            if monthly:
                month[:] = range(1,12+1)
         
            # Write data to dimension variables
            longitude[:]=lon
            latitude[:] =lat
            
            #If saving all time steps
            if len(data) == len(data):
                time[:] = fh_time[:]
            else:
                time[:] = range(1, save_len+1)
                
                   
            if monthly:
                month[:] = range(1,12+1)
         
            #Write data to data variables
            data_dur[:,:,:] = duration    
            data_mag[:,:,:] = rel_intensity
            data_int[:,:,:] = intensity
            data_tim[:,:,:] = timing
            
            if monthly:    
                data_thr[:,:,:] = threshold
            else:
                data_thr[:,:] = threshold
            
            
            # Close the file
            ncfile.close()










