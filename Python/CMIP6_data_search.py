
module use /g/data3/hh5/public/modules
module load conda/analysis3-unstable


import numpy as np 
from clef.code import *
db = connect()
s = Session()


variables=['mrro','pr']
experiments=['historical', 'ssp245', 'ssp585']
timeres=['Amon','Lmon']


#Set search constraints
constraints = {'variable_id': variables, 'table_id': timeres, 'experiment_id': experiments}

results=search(s, project='cmip6', **constraints)



# allvalues=['variable_id']
# fixed=['source_id','member_id']
# 
# #Search results
# res = matching(s, allvalues, fixed, project='CMIP6', local=False, **constraints)
# 

# len(results) 
# results[0]
# results[0]['filenames']     
# results[1]
# 

#res[1] contains search results restricted to common models
#need to save: model, experiment, ensemble member, variable, version, path
search_results=np.zeros((len(res[1]), 6)) * np.nan


for k in range(len(res[1])):
    
    #model
    search_results[k,0]=res[1][k]['source_id']
    
    #experiment
    search_results[k,0]=res[1][k]['']

    #ensemble member 
    res[1][k]['member_id']
    
    #variable

    #version
    
    
    #path





