library(raster)
library(RColorBrewer)
#library(maptools)


#clear R environment
rm(list=ls(all=TRUE))


#Set path
path <- "/g/data1/w35/amu561/CMIP6_drought/" #"/srv/ccrc/data04/z3509830/CMIP6_drought//"


#Source functions
source(paste0(path,"/scripts/R/functions/perc_agree_on_sign_weighted.R"))
source(paste0(path,"/scripts/R/functions/perc_agree_on_mean_weighted.R"))
source(paste0(path,"/scripts/R/functions/perc_agree_with_obs_mean_weighted.R"))
source(paste0(path,"/scripts/R/functions/add_raster_legend.R"))


#Load world shapefile
#world <- readShapePoly(paste0(path, "../World_shapefile/World"))


#Variables
vars <- c("pr")#, "mrro") #list.files(paste0(dr_path, exp[1]))


#List metrics
metrics <- c("mean", "stdev")

proj <- c("CMIP6")#, "CMIP5")

exp <- list(c("ssp245", "ssp585"),
            c("rcp45", "rcp85"))


#Raster 1x1 deg for resampling to common resolution
extent_raster <- raster(nrows=180, ncols=360, xmn=-180, xmx=180, ymn=-90, ymx=90)


#####################
### Plot settings ###
#####################


#Set plot colours

#Historical mean
cols_hist <- colorRampPalette(c("#ffffe5", "#fee391",
                                "#fe9929", "#cc4c02"))


#Future difference
col_opts <- rev(brewer.pal(11, 'RdYlBu'))
col_opts[6] <- "grey80"
cols_diff <- colorRampPalette(col_opts) 

#Set output directory
outdir <- paste0(path, "/Figures/Mean_and_variability_changes/")
dir.create(outdir, recursive=TRUE)


#Set plot limits
lims_hist <- list(mean  = c(0, 10, 20, 30, 40, 50, 60, 1000),
                  stdev = c(0, 10, 20, 30, 40, 50, 60, 10000)
                  )

lims_diff <- list(mean      = c(-1000, -40, -35, -30, -25, -20, -15, -10, -5, 5, 10, 15, 20, 25, 30, 35, 40, 1000),
                  stdev     = c(-10000, -20, -15, -10, -5, -2.5, 2.5, 5, 10, 15, 20, 10000))


#Level of model agreement for stippling (as fraction of models)
agr_level <- 0.75


#Stippling settings
lwd <- 0.1
cex <- 0.15



#Loop through variables
for (v in 1:length(vars)) {
  
  
  #Loop through metrics
  for (m in 1:length(metrics)) {
    
    
    
    ### Set up figure ###
    png(paste0(outdir, "/Mean_changes_in_", vars[v], "_", metrics[m],
               "_CMIP6_CMIP5_Obs.png"),
        height=14, width=16, units="in", res=400)
    
    
    par(mai=c(0.3, 0.3, 0.4, 0.3))
    par(omi=c(0.3, 0.3, 0.6, 0.3))
    
    par(mfrow=c(4, 3))
    
    
    
    ### Get observed data ###
    
    #Find files
    obs_files <- list.files(paste(path, "/Mean_and_variability_change/Observed/", 
                                  "/observed/", vars[v], sep="/"), recursive=TRUE, pattern=metrics[m], full.names=TRUE) 
    
    
    #Load data
    obs_data <- brick(lapply(obs_files, function(x) resample(raster(x), extent_raster)))
    
    #Calculate obs mean
    obs_mean <- mean(obs_data)
    
    #Calculate obs agreement
    obs_agr <- mask(calc(obs_data, perc_agree_on_mean_weighted, weights=NA), obs_mean)
    
    
    
    
    #Loop through projects
    for (p in 1:length(proj)) {
      
      
      #######################
      ### Load model data ###
      #######################
      
      
      #Get historical data
      mod_files_hist <- list.files(paste(path, "/Mean_and_variability_change/", proj[p], 
                                         "/historical/", vars[v], sep="/"), recursive=TRUE,
                                   pattern=metrics[m], full.names=TRUE) 
      
      
      #Get RCP4.5 data
      mod_files_rcp45 <- list.files(paste(path, "/Mean_and_variability_change/", proj[p], 
                                          exp[[p]][1], vars[v], sep="/"), recursive=TRUE,
                                    pattern=metrics[m], full.names=TRUE) 
      
      
      
      
      #Get RCP 8.5 data
      mod_files_rcp85 <- list.files(paste(path, "/Mean_and_variability_change/", proj[p], 
                                          exp[[p]][2], vars[v], sep="/"), recursive=TRUE,
                                    pattern=metrics[m], full.names=TRUE) 
      
      
      #Load data
      mod_data_hist <- brick(lapply(mod_files_hist, function(x) resample(raster(x, stopIfNotEqualSpaced=FALSE), extent_raster)))
      
      #RCP4.5
      ind_early_45 <- which(grepl("_2015_2050_", mod_files_rcp45))
      
      if(proj[p]=="CMIP6") {
        ind_late_45 <- which(grepl("_2051_2100_", mod_files_rcp45)) 
        
      } else if (proj[p] == "CMIP5"){
        ind_late_45 <- which(grepl("_2051_2100_", mod_files_rcp45)) 
      }
      
      mod_data_rcp45_2050 <- brick(lapply(mod_files_rcp45[ind_early_45], function(x) resample(raster(x, stopIfNotEqualSpaced=FALSE), extent_raster))) #first period
      mod_data_rcp45_2100 <- brick(lapply(mod_files_rcp45[ind_late_45], function(x) resample(raster(x, stopIfNotEqualSpaced=FALSE), extent_raster))) #second period
      
      
      #RCP8.5
      ind_early_85 <- which(grepl("_2015_2050_", mod_files_rcp85))
      
      if(proj[p]=="CMIP6") {
        ind_late_85 <- which(grepl("_2051_2100_", mod_files_rcp85)) 
        
      } else if (proj[p] == "CMIP5"){
        ind_late_85 <- which(grepl("_2051_2099_", mod_files_rcp85)) 
      }
      
      mod_data_rcp85_2050 <- brick(lapply(mod_files_rcp85[ind_early_85], function(x) resample(raster(x,stopIfNotEqualSpaced=FALSE), extent_raster))) #first period
      mod_data_rcp85_2100 <- brick(lapply(mod_files_rcp85[ind_late_85], function(x) resample(raster(x,stopIfNotEqualSpaced=FALSE), extent_raster))) #second period
      
      
      
      ##############################
      ### Work out model weights ###
      ##############################
      
      #Work these out separately for now in case mismatches in models
      
      #Historical
      models_hist <- sapply(mod_files_hist, function(x) strsplit(strsplit(x, paste0(metrics[m],"_pr_"))[[1]][2], "_")[[1]][1])
      unique_hist <- unique(models_hist)
      
      weights_hist <- vector(length=length(models_hist))
      
      for (u in 1:length(unique_hist)) { 
        ind <- which(models_hist == unique_hist[u])
        weights_hist[ind] <- 1 / length(ind)
      }
      
      
      #RCP4.5
      models_rcp45 <- sapply(mod_files_rcp45[ind_early_45], function(x) strsplit(strsplit(x, paste0(metrics[m],"_pr_"))[[1]][2], "_")[[1]][1])
      unique_rcp45 <- unique(models_rcp45)
      
      weights_rcp45 <- vector(length=length(models_rcp45))
      
      for (u in 1:length(unique_rcp45)) { 
        ind <- which(models_rcp45 == unique_rcp45[u])
        weights_rcp45[ind] <- 1 / length(which(models_rcp45 == unique_rcp45[u])) 
      }
      
      
      #RCP8.5
      models_rcp85 <- sapply(mod_files_rcp85[ind_early_85], function(x) strsplit(strsplit(x, paste0(metrics[m],"_pr_"))[[1]][2], "_")[[1]][1])
      unique_rcp85 <- unique(models_rcp85)
      
      weights_rcp85 <- vector(length=length(models_rcp85))
      
      for (u in 1:length(unique_rcp85)) { 
        ind <- which(models_rcp85 == unique_rcp85[u])
        weights_rcp85[ind] <- 1/ length(which(models_rcp85 == unique_rcp85[u])) 
      }
      
      
      
      ################
      ### Plotting ###
      ################
      
      #Add line separating CMIP6/CMIP5
      if(p ==2) {
        
        par(mai=c(0.3, 0.3, 0.7, 0.3))
        
        lines(c(-100000, 30000), c(-125, -125), xpd=NA)
      }
      
      
      
      #--- Historical model mean (stippling where 75% of models within 10% of obs mean) ---#
      
      hist_plot_data <- weighted.mean(mod_data_hist, w=weights_hist, na.rm=TRUE)
      
      #Mask with obs
      hist_plot_data <- mask(hist_plot_data, obs_mean)
      
      
      #Plot
      plot(hist_plot_data, breaks=lims_hist[[metrics[m]]], col=cols_hist(length(lims_hist[[metrics[m]]])-1),
           ylab="", xlab="", yaxt="n", xaxt="n", legend=FALSE)
      
      #Main title
      mtext(side=3, line=0, text=paste0(proj[p], " (n = ", sum(weights_hist), ")"), xpd=NA)
      
      #Time period label
      mtext(side=3, line=3, font=2, text=paste0("Historical mean ", metrics[m], " 1950-2014"), xpd=NA)
      
      
      #Legend
      len <- length(lims_hist[[metrics[m]]])-1
      add_raster_legend2(cols=cols_hist(len), limits=lims_hist[[metrics[m]]][2:len],
                         main_title="", plot_loc=c(0.12,0.88,0.07,0.10), spt.cex=1)
      
      
      #Stippling (at least 75% of models within 10% of obs)
      hist_mod_agr <- calc(brick(list(obs_mean, mod_data_hist)), function(x) 
        perc_agree_with_obs_mean_weighted(x, weights=weights_hist))
      
      
      
      #Find pixels where lag-1 autocore greated than CI
      ind    <- which(values(hist_mod_agr) >= agr_level)
      coords <- coordinates(hist_mod_agr)
      
      #Add stippling
      points(coords[ind,1], coords[ind,2], pch=20, lwd=lwd, cex=cex)
      
      
      
      
      
      
      #--- Future mean difference (2015-2050), RCP 4.5 ---#
      
      future_data <- list(mod_data_rcp45_2050, mod_data_rcp45_2100)
      labs <- c("2015-2050", "2051-2100")
      
      for(f in 1:length(future_data)) {
        
        fut_plot_data <- weighted.mean(future_data[[f]], w=weights_rcp45, na.rm=TRUE) - hist_plot_data
        
        
        #Plot
        plot(fut_plot_data, breaks=lims_diff[[metrics[m]]], col=cols_diff(length(lims_diff[[metrics[m]]])-1),
             ylab="", xlab="", yaxt="n", xaxt="n", legend=FALSE)
        
        #Main title
        mtext(side=3, line=0, text=paste0(proj[p], " RCP4.5 (n = ", sum(weights_rcp45), ")"), xpd=NA)
        
        #Time period label
        mtext(side=3, line=3, font=2, text=paste0("Future difference ", labs[f]), xpd=NA)
        
        
        #Legend
        len <- length(lims_diff[[metrics[m]]])-1
        add_raster_legend2(cols=cols_diff(len), limits=lims_diff[[metrics[m]]][2:len],
                           main_title="", plot_loc=c(0.12,0.88,0.07,0.10), spt.cex=1)
        
        
        #Stippling (at least 75% of models within 10% of obs)
        fut_mod_agr <- calc(future_data[[f]] - hist_plot_data, function(x) perc_agree_on_sign_weighted(x, weights=weights_rcp45))
        
        
        
        #Find pixels where lag-1 autocore greated than CI
        ind    <- which(values(fut_mod_agr) >= agr_level)
        coords <- coordinates(fut_mod_agr)
        
        #Add stippling
        points(coords[ind,1], coords[ind,2], pch=20, lwd=lwd, cex=cex)
        
        
        
        
      }
      
      #Reset par
      if(p==2) par(mai=c(0.3, 0.3, 0.4, 0.3))
      
      
      #--- Historical observed mean ---#
      
      #Plot
      plot(obs_mean, breaks=lims_hist[[metrics[m]]], col=cols_hist(length(lims_hist[[metrics[m]]])-1),
           ylab="", xlab="", yaxt="n", xaxt="n", legend=FALSE)
      
      #Main title
      mtext(side=3, line=0, text=paste0("Observed (n = ", nlayers(obs_data), ")"), xpd=NA)
      
      #Legend
      len <- length(lims_hist[[metrics[m]]])-1
      add_raster_legend2(cols=cols_hist(len), limits=lims_hist[[metrics[m]]][2:len],
                         main_title="", plot_loc=c(0.12,0.88,0.07,0.10), spt.cex=1)
      
      
      #Find pixels where lag-1 autocore greated than CI
      ind    <- which(values(obs_agr) == 1)
      coords <- coordinates(obs_agr)
      
      #Add stippling
      points(coords[ind,1], coords[ind,2], pch=20, lwd=lwd, cex=cex)
      
      
      
      
      
      
      #--- Future mean difference (2015-2050), RCP 8.5 ---#
      
      future_data <- list(mod_data_rcp85_2050, mod_data_rcp85_2100)
      
      
      for(f in 1:length(future_data)) {
        
        fut_plot_data <- weighted.mean(future_data[[f]], w=weights_rcp85, na.rm=TRUE) - hist_plot_data
        
        
        #Plot
        plot(fut_plot_data, breaks=lims_diff[[metrics[m]]], col=cols_diff(length(lims_diff[[metrics[m]]])-1),
             ylab="", xlab="", yaxt="n", xaxt="n", legend=FALSE)
        
        #Main title
        mtext(side=3, line=0, text=paste0(proj[p], " RCP8.5 (n = ", sum(weights_hist), ")"), xpd=NA)
        
        #Legend
        len <- length(lims_diff[[metrics[m]]])-1
        add_raster_legend2(cols=cols_diff(len), limits=lims_diff[[metrics[m]]][2:len],
                           main_title="", plot_loc=c(0.12,0.88,0.07,0.10), spt.cex=1)
        
        
        #Stippling (at least 75% of models within 10% of obs)
        fut_mod_agr <- calc(future_data[[f]] - hist_plot_data, function(x) perc_agree_on_sign_weighted(x, weights=weights_rcp85))
        
        
        
        #Find pixels where lag-1 autocore greated than CI
        ind    <- which(values(fut_mod_agr) >= agr_level)
        coords <- coordinates(fut_mod_agr)
        
        #Add stippling
        points(coords[ind,1], coords[ind,2], pch=20, lwd=lwd, cex=cex)
        
        
      }
      
      
    } #projects
    
    dev.off()
  } #metrics
  
} #variables




