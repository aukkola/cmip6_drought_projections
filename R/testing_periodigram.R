#library(TSA)
library(lomb)

region="Amazon"

path <- "~/Documents/CMIP6_drought_projections/"

data = read.csv(paste0(path,"/Timeseries_mean_var/CMIP6/ssp585/pr/Timeseries_mean_pr_CMIP6_ssp585_", 
                       region, ".csv"), header=TRUE)

dr_data <- read.csv(paste0(path,"/Timeseries/CMIP6/ssp585/pr/Perc_15/Baseline_1950_2014/Scale_3/",
                          "/duration/Timeseries_pr_CMIP6_ssp585_duration_", region, ".csv"),
                    header=TRUE)

models <- data[,1]
data   <- data[,2:ncol(data)]

dr_data <- dr_data[,2:ncol(dr_data)]


#Plot first ensemble of each model
mods <- unique(models)

par(mfrow=c(4,4))
par(mai=c(0.2, 0.2, 0.2, 0.2))

#initialise
p_hist <- list()
p_fut <- list()


for (k in 1:length(mods)) {
  
  #Index for first ensemble
  ind <- which(models == mods[k])[1]
  
  x <- unlist(data[ind,])
  
  
  hist_start <- which(colnames(data)== "X1950")
  hist_end   <- which(colnames(data)== "X2014.11")
  
  fut_start <-  which(colnames(data)== "X2050")
  fut_end   <-  length(x) 
  
  
  
  x_hist <- x[hist_start:hist_end]
  x_fut  <- x[fut_start:fut_end]
  
  #Calculate mean drought change
  mean_dr <- mean(unlist(dr_data[ind, fut_start:fut_end]), na.rm=TRUE) -
             mean(unlist(dr_data[ind, hist_start:hist_end]), na.rm=TRUE)

  
  
  #Historical
  p_hist = lsp(x_hist, alpha=0.1, plot=FALSE, ofac=4) #periodogram(x_hist, plot=FALSE)
  #p_hist = randlsp(x=x_hist, alpha=0.05, plot=FALSE, ofac=4) #periodogram(x_hist, plot=FALSE)
  
  time_hist = 1/p_hist$scanned
  
  start_hist <- 90 #13
  end_hist <- length(time_hist)
  
  
  #Future
  p_fut = lsp(x_fut, alpha=0.1, plot=FALSE, ofac=4) #periodogram(x_fut, plot=FALSE)
 # p_fut = randlsp(x=x_fut, alpha=0.05, plot=FALSE, ofac=4) #periodogram(x_fut, plot=FALSE)
  
  time_fut = 1/p_fut$scanned
  
  start_fut <- 90 #85
  end_fut   <- length(time_fut)
  
  
  ### Plotting ###
  
  #Initialise
  plot(range(c(time_hist[start_hist:end_hist], time_fut[start_fut:end_fut])),
       range(c(p_hist$power[start_hist:end_hist], p_hist$power[start_fut:end_fut], p_hist$alpha, p_fut$alpha)),
       type="n", ylab="power", xlab="frequency (months)", ylim=c(0, 60))
  
  
  #Change in drought metric
  mtext(side=3, round(mean_dr, 2), adj=1, line=-1)
  
  #Historical
  #tseries
  lines(time_hist[start_hist:end_hist], p_hist$power[start_hist:end_hist], type='l', main=mods[k])
  
  #Significance line
  lines(time_hist[start_hist:end_hist], rep(p_hist$sig.level, length(time_hist[start_hist:end_hist])), lty=2)
  
  
  #Future
  #tseries
  lines(time_fut[start_fut:end_fut], p_fut$power[start_fut:end_fut], 
        type='l', main=mods[k], col="red")
  
  #Significance line
  #lines(time_fut[start_fut:end_fut], p_fut$random.peaks[start_fut:end_fut], col="red", lty=2)
  lines(time_fut[start_fut:end_fut], rep(p_fut$sig.level, length(time_fut[start_fut:end_fut])),
        lty=2, col="red")
  
}


# n <- length(x)
# FF = abs(fft(x)/sqrt(128))^2
# P = (4/n)*FF # Only need the first (n/2)+1 values of the FFT result.
# f = (0:(n-1))/n # this creates harmonic frequencies from 0 to .5 in steps of 1/128.
# plot(f, P, type="l") # This plots the
# #periodogram; type = “l” creates a line
# #plot. Note: l is lowercase L, not number 1.
# 
# N = length(x)
# I = abs(fft(x)/sqrt(N))^2
# ## Scale periodogram
# P = (4/N) * I  ## scaled periodogram
# f = (0:floor(N/2))/N
# plot(f, I[1:((N/2) + 1)], type = "o", xlab = "frequency", ylab = "")
# 
# 
# 
# spec.pgram(x, detrend = FALSE, ci=0, col = "blue", main = "without (blue) and with (red) trend removal")
# 
# 

#p_hist = randlsp(x=x_hist, alpha=0.05, plot=TRUE, type="period", ofac=4) #periodogram(x_hist, plot=FALSE)


