library(raster)
library(RColorBrewer)
library(maptools)
library(grDevices)
library(zoo)

#clear R environment
rm(list=ls(all=TRUE))


#Set path
path <- "/g/data1/w35/amu561/CMIP6_drought/" #"/srv/ccrc/data04/z3509830/CMIP6_drought//"


#Source functions
source(paste0(path,"/scripts/R/functions/add_raster_legend.R"))
source(paste0(path,"/scripts/R/functions/wtd_cor.R"))


#Load world shapefile
world <- readShapePoly(paste0(path, "../World_shapefile/World"))


#Variables
vars <- c("pr")#, "mrro") #list.files(paste0(dr_path, exp[1]))


#List metrics
metrics <- c("mean", "stdev")

proj <- c("CMIP6")#, "CMIP5")

exp <- list(c("ssp585"))#,
# c("rcp85"))


#Raster 1x1 deg for resampling to common resolution
extent_raster  <- raster(nrows=180, ncols=360, xmn=-180, xmx=180, ymn=-90, ymx=90)
stipple_raster <- raster(nrows=180/2, ncols=360/2, xmn=-180, xmx=180, ymn=-90, ymx=90)



#####################
### Plot settings ###
#####################


#Set plot colours

# cols_diff <- colorRampPalette(c("#8c510a", "#d8b365", "#f6e8c3", "#f5f5f5",
#                "#c7eae5", "#5ab4ac", "#01665e"))

#Future difference
#Future difference
col_opts <- brewer.pal(11, 'RdYlBu')
col_opts[6] <- "grey80"
cols_diff <- colorRampPalette(col_opts) 


#Set output directory
outdir <- paste0(path, "/Figures/Paper_figures/")
dir.create(outdir, recursive=TRUE)



lims_diff <- c(-1, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3, -0.2, 
               0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1)


#Level of model agreement for stippling (p-value)
agr_level <- 0.05


#Stippling settings
lwd <- 0.1
cex <- 0.15

panel_cex=0.7




###############################################
### Correlation mean vs. standard deviation ###
###############################################


#Loop through variables
for (v in 1:length(vars)) {
  
   
  png(paste0(outdir, "/Correlation_", vars[v], "_future_change_in_mean_and_stdev",
             "_CMIP6.png"),
      height=5, width=8.27, units="in", res=400)
  
  
  par(mai=c(0.3, 0.1, 0.3, 0.1))
  par(omi=c(0.1, 0.1, 0, 0.1))
  par(bty="n")
  
  #layout(matrix(c(1,1,1,2,2,2, 3,3,4,4,5,5), nrow=2, byrow=TRUE))
  
  
  #Initialise
  future_diff <- list()
  
  
  #Loop through metrics
  for (m in 1:length(metrics)) {
 
    
    ### Get obs data for masking ###
    
    #Find files
    obs_files <- list.files(paste(path, "/Mean_and_variability_change/Observed/", 
                                  "/observed/", vars[v], sep="/"), recursive=TRUE, pattern=metrics[m], full.names=TRUE) 
    
    
    #Load data
    obs_data <- brick(lapply(obs_files, function(x) resample(raster(x), extent_raster)))
    
    #Calculate obs mean
    obs_mean <- crop(mean(obs_data), extent(c(-180, 180, -65, 85)))
    
    
    
    

    p=1
    
    #######################
    ### Load model data ###
    #######################
    
    models <- c("BCC-CSM2-MR", "CESM2-WACCM", "CNRM-CM6-1",
                "CanESM5", "GFDL-CM4", "IPSL-CM6A-LR", "MIROC6",
                "MRI-ESM2-0", "UKESM1-0-LL")
    
    
    #Get historical data
    mod_files_hist <- list.files(paste(path, "/Mean_and_variability_change/", proj[p], 
                                       "/historical/", vars[v], sep="/"), recursive=TRUE,
                                 pattern=metrics[m], full.names=TRUE) 
    
    #Extract models
    mod_files_hist <- unlist(sapply(models, function(x) mod_files_hist[which(grepl(x, mod_files_hist))]))
    
    
    
    #Get RCP 8.5 data
    mod_files_rcp85 <- list.files(paste(path, "/Mean_and_variability_change/", proj[p], 
                                        exp[[p]], vars[v], sep="/"), recursive=TRUE,
                                  pattern=metrics[m], full.names=TRUE) 
    
    #Extract models
    mod_files_rcp85 <- unlist(sapply(models, function(x) mod_files_rcp85[which(grepl(x, mod_files_rcp85))]))
    
    
    #Load data
    mod_data_hist <- brick(lapply(mod_files_hist, function(x) resample(raster(x, stopIfNotEqualSpaced=FALSE), extent_raster)))
    
    
    
    #RCP8.5 (take latter time period)
    ind_late_85  <- which(grepl("_2051_2100_", mod_files_rcp85)) 
    
    #Read data and resample to the same resolution
    mod_data_rcp85_2100 <- brick(lapply(mod_files_rcp85[ind_late_85], function(x) 
                                 resample(raster(x,stopIfNotEqualSpaced=FALSE), extent_raster))) #second period
    
    
    
    ##############################
    ### Work out model weights ###
    ##############################
    
    #Work these out separately for now in case mismatches in models
    
    #Historical
    models_hist <- sapply(mod_files_hist, function(x) strsplit(strsplit(x, paste0(metrics[m],"_pr_"))[[1]][2], "_")[[1]][1])
    unique_hist <- unique(models_hist)
    
    weights_hist <- vector(length=length(models_hist))
    
    for (u in 1:length(unique_hist)) { 
      ind <- which(models_hist == unique_hist[u])
      weights_hist[ind] <- 1 / length(ind)
    }
    
    
    
    #RCP8.5
    models_rcp85 <- sapply(mod_files_rcp85[ind_late_85], function(x) strsplit(strsplit(x, paste0(metrics[m],"_pr_"))[[1]][2], "_")[[1]][1])
    unique_rcp85 <- unique(models_rcp85)
    
    weights_rcp85 <- vector(length=length(models_rcp85))
    
    for (u in 1:length(unique_rcp85)) { 
      ind <- which(models_rcp85 == unique_rcp85[u])
      weights_rcp85[ind] <- 1/ length(which(models_rcp85 == unique_rcp85[u])) 
    }
    
    #Check that historical and future weights are of equal length
    if (length(weights_hist) != length(weights_rcp85)) stop("weights don't match")
    
    
    #Calculate future difference
    future_diff[[m]] <- mod_data_rcp85_2100 - mod_data_hist
    
    
  } #metrics
  
  
  
  ################
  ### Plotting ###
  ################
  

  #Calculate weighted correlation of future difference in mean vs standard deviation
  
  #Check that mean and st dev bricks are the same size
  if (nlayers(future_diff[[1]]) != nlayers(future_diff[[2]])) stop("mean and st dev don't match")
  
  #Calculate correlation
  correl <- calc(brick(future_diff), fun = function(x) wtd_cor_per_pixel(x, weights=weights_rcp85))
  
  
  
  #Crop data
  correl <- mask(crop(correl, extent(c(-180, 180, -65, 85))), obs_mean)
  
 
  #Plot
  # plot(fut_plot_data, breaks=lims_diff[[metrics[m]]], col=cols_diff(length(lims_diff[[metrics[m]]])-1),
  #      ylab="", xlab="", yaxt="n", xaxt="n", legend=FALSE)
  # 
  image(correl[[1]], breaks=lims_diff, col=cols_diff(length(lims_diff)-1),
        axes=FALSE, ann=FALSE, asp=1, xlim=c(-180, 180), ylim=c(-65, 85))
  
  #World shapefile
  plot(world, border="grey50", add=TRUE, lwd=0.7)
  
  #Main title
  mtext(side=3, line=0, text="Correlation (mean vs. stdev)", xpd=NA)      #main title
  #mtext(side=3, line=0, adj=0, text="a", font=2, xpd=NA, cex=panel_cex) #panel number

  #Legend
  len <- length(lims_diff)-1
  
  add_raster_legend2(cols=cols_diff(len), limits=lims_diff[2:len],
                     main_title="(-)", plot_loc=c(0.12,0.88, -0.03, 0), 
                     title.cex=1, spt.cex=1, clip=TRUE, ysp_title_old=TRUE)
  
  
  #Stippling (p-value <= 0.05)
  fut_mod_agr <- correl[[2]]
  
  
  #Resample stippling to make it larger
  fut_mod_agr <- resample(fut_mod_agr, stipple_raster)
  
  #Find pixels where lag-1 autocore greated than CI
  ind    <- which(values(fut_mod_agr) <= agr_level)
  coords <- coordinates(fut_mod_agr)
  
  #Add stippling
  points(coords[ind,1], coords[ind,2], pch=20, lwd=lwd, cex=cex)
  

    
  dev.off()
  
} #vars
  



####################
### Scatter plot ###
####################

x <- as.vector(values(future_diff[[1]]))
y <- as.vector(values(future_diff[[2]]))



png(paste0(outdir, "/FigureSX_scatterplot_mean_vs_standard_deviation_change.png"),
    height=8, width=8, units="in", res=300)

par(omi=c(0.2, 0.2, 0.2, 0.2))
par(mai=c(1.02, 0.92, 0.82, 0.42))


plot(x, y, xlab=expression("Change in mean ("~"mm"~"month"^"-1"*")"), 
     ylab=expression("Change in standard deviation ("~"mm"~"month"^"-1"*")"),
     ylim=c(min(c(x,y), na.rm=TRUE), 300), xlim=c(min(c(x,y), na.rm=TRUE), 300), pch=20, cex=0.1,
     col=adjustcolor(col_opts[length(col_opts)-1], alpha.f=0.4),
     cex.lab = 1.2)


lm <- lm(y ~ x)

pred_y <- lm$coefficients[2] * x + lm$coefficients[1]

lines(x, pred_y, col=col_opts[length(col_opts)])

lines(range(x,y, na.rm=TRUE), range(x,y, na.rm=TRUE), lty=2)

legend(x=-150, y=300, paste0("y = ", round(lm$coefficients[2], digits=2), " * x + ", round(lm$coefficients[1], digits=2)),
     cex=1.2, bty="n")

legend(x=-150, y=270, bquote("R"^"2"~"= "~.(round(summary(lm)$r.squared, digits=3))), cex=1.2, bty="n")



dev.off()









##################################################
### Map mean vs. standard deviation difference ###
##################################################


lims_diff <- c(-50, -40, -30, -25, -20, -15, -10, -5, -2.5, 
               2.5, 5, 10, 15, 20, 25, 30, 40, 50)


#Loop through variables
for (v in 1:length(vars)) {
  
  
  # png(paste0(outdir, "/Difference_", vars[v], "_future_change_in_mean_and_stdev",
  #            "_CMIP6.png"),
  #     height=5, width=8.27, units="in", res=400)
  # 
  
  par(mai=c(0.3, 0.1, 0.3, 0.1))
  par(omi=c(0.1, 0.1, 0, 0.1))
  par(bty="n")
  
  #layout(matrix(c(1,1,1,2,2,2, 3,3,4,4,5,5), nrow=2, byrow=TRUE))
  
  
  #Initialise
  future_diff <- list()
  
  
  #Loop through metrics
  for (m in 1:length(metrics)) {
    
    
    ### Get obs data for masking ###
    
    #Find files
    obs_files <- list.files(paste(path, "/Mean_and_variability_change/Observed/", 
                                  "/observed/", vars[v], sep="/"), recursive=TRUE, pattern=metrics[m], full.names=TRUE) 
    
    
    #Load data
    obs_data <- brick(lapply(obs_files, function(x) resample(raster(x), extent_raster)))
    
    #Calculate obs mean
    obs_mean <- crop(mean(obs_data), extent(c(-180, 180, -65, 85)))
    
    
    p=1
    
    #######################
    ### Load model data ###
    #######################
    
    models <- c("BCC-CSM2-MR", "CESM2-WACCM", "CNRM-CM6-1",
                "CanESM5", "GFDL-CM4", "IPSL-CM6A-LR", "MIROC6",
                "MRI-ESM2-0", "UKESM1-0-LL")
    
    
    #Get historical data
    mod_files_hist <- list.files(paste(path, "/Mean_and_variability_change/", proj[p], 
                                       "/historical/", vars[v], sep="/"), recursive=TRUE,
                                 pattern=metrics[m], full.names=TRUE) 
    
    #Extract models
    mod_files_hist <- unlist(sapply(models, function(x) mod_files_hist[which(grepl(x, mod_files_hist))]))
    
    
    
    #Get RCP 8.5 data
    mod_files_rcp85 <- list.files(paste(path, "/Mean_and_variability_change/", proj[p], 
                                        exp[[p]], vars[v], sep="/"), recursive=TRUE,
                                  pattern=metrics[m], full.names=TRUE) 
    
    #Extract models
    mod_files_rcp85 <- unlist(sapply(models, function(x) mod_files_rcp85[which(grepl(x, mod_files_rcp85))]))
    
    
    #Load data
    mod_data_hist <- brick(lapply(mod_files_hist, function(x) resample(raster(x, stopIfNotEqualSpaced=FALSE), extent_raster)))
    
    
    
    #RCP8.5 (take latter time period)
    ind_late_85  <- which(grepl("_2051_2100_", mod_files_rcp85)) 
    
    #Read data and resample to the same resolution
    mod_data_rcp85_2100 <- brick(lapply(mod_files_rcp85[ind_late_85], function(x) 
      resample(raster(x,stopIfNotEqualSpaced=FALSE), extent_raster))) #second period
    
    
    
    ##############################
    ### Work out model weights ###
    ##############################
    
    #Work these out separately for now in case mismatches in models
    
    #Historical
    models_hist <- sapply(mod_files_hist, function(x) strsplit(strsplit(x, paste0(metrics[m],"_pr_"))[[1]][2], "_")[[1]][1])
    unique_hist <- unique(models_hist)
    
    weights_hist <- vector(length=length(models_hist))
    
    for (u in 1:length(unique_hist)) { 
      ind <- which(models_hist == unique_hist[u])
      weights_hist[ind] <- 1 / length(ind)
    }
    
    
    
    #RCP8.5
    models_rcp85 <- sapply(mod_files_rcp85[ind_late_85], function(x) strsplit(strsplit(x, paste0(metrics[m],"_pr_"))[[1]][2], "_")[[1]][1])
    unique_rcp85 <- unique(models_rcp85)
    
    weights_rcp85 <- vector(length=length(models_rcp85))
    
    for (u in 1:length(unique_rcp85)) { 
      ind <- which(models_rcp85 == unique_rcp85[u])
      weights_rcp85[ind] <- 1/ length(which(models_rcp85 == unique_rcp85[u])) 
    }
    
    #Check that historical and future weights are of equal length
    if (length(weights_hist) != length(weights_rcp85)) stop("weights don't match")
    
    
    #Calculate future difference
    future_diff[[m]] <- mod_data_rcp85_2100 - mod_data_hist
    
    
  } #metrics
  
  
  
  ################
  ### Plotting ###
  ################
  
  
  #Calculate weighted correlation of future difference in mean vs standard deviation
  
  #Check that mean and st dev bricks are the same size
  
  plot_data <- weighted.mean(future_diff[[1]], weights_rcp85) - 
               weighted.mean(future_diff[[2]], weights_rcp85)
  
  
  #Crop data
  plot_data <- mask(crop(plot_data, extent(c(-180, 180, -65, 85))), obs_mean)
  
  
  #Plot
  # plot(fut_plot_data, breaks=lims_diff[[metrics[m]]], col=cols_diff(length(lims_diff[[metrics[m]]])-1),
  #      ylab="", xlab="", yaxt="n", xaxt="n", legend=FALSE)
  # 
  image(plot_data, breaks=lims_diff, col=cols_diff(length(lims_diff)-1),
        axes=FALSE, ann=FALSE, asp=1, xlim=c(-180, 180), ylim=c(-65, 85))
  
  #World shapefile
  plot(world, border="grey50", add=TRUE, lwd=0.7)
  
  #Main title
  mtext(side=3, line=0, text="Mean - st dev", xpd=NA)      #main title
  #mtext(side=3, line=0, adj=0, text="a", font=2, xpd=NA, cex=panel_cex) #panel number
  
  #Legend
  len <- length(lims_diff)-1
  
  add_raster_legend2(cols=cols_diff(len), limits=lims_diff[2:len],
                     main_title="(-)", plot_loc=c(0.12,0.88, -0.03, 0), 
                     title.cex=1, spt.cex=1, clip=TRUE, ysp_title_old=TRUE)
  
  
  # #Stippling (p-value <= 0.05)
  # fut_mod_agr <- correl[[2]]
  # 
  # 
  # #Resample stippling to make it larger
  # fut_mod_agr <- resample(fut_mod_agr, stipple_raster)
  # 
  # #Find pixels where lag-1 autocore greated than CI
  # ind    <- which(values(fut_mod_agr) <= agr_level)
  # coords <- coordinates(fut_mod_agr)
  # 
  # #Add stippling
  # points(coords[ind,1], coords[ind,2], pch=20, lwd=lwd, cex=cex)
  # 
  
  
 # dev.off()
  
} #vars



