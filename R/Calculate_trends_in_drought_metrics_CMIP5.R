library(raster)


#clear R environment
rm(list=ls(all=TRUE))


#Set path
path <- "/g/data1/w35/amu561/CMIP6_drought/"


#Source functions
source(paste0(path,"/scripts/R/functions/trend_per_pixel.R"))



#Metrics
metrics <- c("duration", "intensity", "rel_intensity")#, "frequency")


#Experiments
experiments <- c("historical", "rcp45", "rcp85")

#Variables
variables <- c("pr", "mrro", "mrros")

#Set percentile
percentile <- "Perc_15"

#Set baseline
baseline <- "Baseline_1950_2014"

#Set scale
scale <- "Scale_3"


#Set years to use for calculating trend
#(match with experiments)
years <- list(c(1950, 2014),
              c(2015, 2099),
              c(2015, 2099))


#Loop through experiments
for (e in 1:length(experiments)) {
  
  print(paste0("Experiment ", e, "/", length(experiments)))
  
  
  #Loop through variables
  for(v in 1:length(variables)) {
    
      
    models <- list.files(paste(path, "/Drought_metrics_CMIP5", experiments[e], variables[v], 
                               percentile, baseline, scale, sep="/"))
    
    
    
    #Loop through models
    for (mod in 1:length(models)){
      
      print(paste0("Model ", mod, "/", length(models)))
      
      
      #Find files (may contain several ensembles)
      files <- list.files(paste(path, "/Drought_metrics_CMIP5", experiments[e], variables[v], 
                                percentile, baseline, scale, models[mod], sep="/"), full.names=TRUE)
      
      
      #If didn't find any files, skip
      if(length(files) == 0) next
      
      
      #Output directory
      outdir <- paste0(paste(path, "/Drought_metrics_CMIP5/Trends/", experiments[e],
                             variables[v], percentile, baseline, scale, models[mod], sep="/"))
      dir.create(outdir, recursive=TRUE)
      
      
      #Loop through metrics
      for (m in 1:length(metrics)) {
        
        
        #Load data
        if (metrics[m] == "frequency") {
          data <- lapply(files, brick, varname="duration", stopIfNotEqualSpaced=FALSE)
          
          data <- lapply(data, function(x) calc(x, calculate_frequency))
          
        } else { 
          data <- lapply(files, brick, varname=metrics[m], stopIfNotEqualSpaced=FALSE)
        }
        

        
        

        #Write output
        for (f in 1:length(files)) {
          
          print(paste0("Ensemble ", f, "/", length(files)))
          
          fname <- paste0(outdir, "/Trend_", years[[e]][1], "_", years[[e]][2], "_", 
                          metrics[m], "_", basename(files[f]))
          
          
          #Check if output already exists, skip
          if (file.exists(fname)) next
          
          
          #Extract desired time period
          tstamps <- names(data[[f]])
          
          start_ind <- which(grepl(years[[e]][1], tstamps))[1]
          end_ind   <- tail(which(grepl(years[[e]][2], tstamps)), n=1)
          
          if (length(start_ind) ==0 | length(end_ind) == 0) {
            print(paste0("Skipping ", files[f], ", time stamps not correct !!!!!"))
            next
          }
          
          data[[f]] <- data[[f]][[start_ind:end_ind]]
          
          
          #Calculate Mann-Kendall trend
          trend <- calc(data[[f]], trend_per_pixel)#_MannKen))
          
          
          
          writeRaster(trend, fname, varname="trend", longname=paste0("trend in ", 
                                                          metrics[m]), overwrite=TRUE)
          
          
        }
        
      } #metrics
    } #models    
  } #variables
} #experiments

