library(rasterVis)
library(RColorBrewer)
library(raster)
library(rgdal)
library(maptools)

#clear R environment
rm(list=ls(all=TRUE))


#Set path
path <- "/srv/ccrc/data04/z3509830/CMIP6_drought//"


#Source functions
source(paste0(path,"/scripts/R/functions/trend_per_pixel.R"))
source(paste0(path,"/scripts/R/functions/perc_agree_on_sign.R"))
source(paste0(path,"/scripts/R/functions/add_raster_legend.R"))


#Load world shapefile
world <- readShapePoly(paste0(path, "../World_shapefile/World"))



#Set percentile
percentile <- 10


#Variables
vars <- c("pr", "mrro") #list.files(paste0(dr_path, exp[1]))



#Project and experiment names
proj <- c("CMIP6", "CMIP5")#, "Observed")

exp <- list(c("historical"), c("historical"))#, c("observed"))



#Set plot colours
cols <- colorRampPalette(brewer.pal(11, 'RdYlBu'))



#Set output directory
outdir <- paste0(path, "/Figures/Mean_trends/")
dir.create(outdir, recursive=TRUE)


#Set plot limits
lims <- list(pr    = c(-1000, -1, -0.5, -0.25, -0.1, 0.1, 0.25, 0.5, 1, 1000) /500,
             mrro  = c(-1000, -1, -0.5, -0.25, -0.1, 0.1, 0.25, 0.5, 1, 1000) /500,
             mrros = c(-1000, -1, -0.5, -0.25, -0.1, 0.1, 0.25, 0.5, 1, 1000) /500)


#Level of model agreement for stippling (as fraction of models)
agr_level <- 0.8


#Raster 1x1 deg for resampling to common resolution
extent_raster <- raster(nrows=180, ncols=360, xmn=-180, xmx=180, ymn=-90, ymx=90 )



#Loop through variables
for (v in 1:length(vars)) {
  

    #########################
    ### Initialise figure ###
    #########################
    
    
    png(paste0(outdir, "/Historical_trend_in_mean_", vars[v], "_CMIP6_and_CMIP5_and_Obs.png"),
        height=7, width=12, units="in", res=400)
    
    par(mfrow=c(2,2))
    par(mai=c(0.2, 0.2, 0.2, 0.2))
    par(omi=c(0.3, 0.2, 0.3, 0.2))
    
    
    
    #######################
    ### CMIP6 vs. CMIP5 ###
    #######################
    
    
    for (p in 1:length(proj)) {
      
      dr_path <- paste0(path, "/Mean_trends/", proj[p])
      
      
      #Find models
      models <- list.files(paste0(dr_path, "/", exp[[p]][1], "/",  vars[v]))
      
      
      if (any(grepl("GFDL", models))) {
        models <- models[-which(grepl("GFDL", models))]
      }
      
      
      
      ### Calculate median trend ###  
      
      #Initialise
      median_trend  <- list()
      var_data      <- list()
      
      for (mod in 1:length(models)) {
        
        #All model files
        files <- list.files(paste0(dr_path, "/", exp[[p]], "/",  vars[v], "/", 
                                   models[mod]), full.names=TRUE,
                            pattern="Trend_")[1] #Important: taking first ensemble member here !!!!!!!!!!!!!!!!
        
          
        var_data[[mod]] <- resample(brick(files, varname="trend"), extent_raster)
          
        
        
      }
      
      ### !!!!!!!!!!!!!!!!!!!!! Need to change this (na.rm, use some threshold)
      median_trend <- calc(brick(lapply(var_data, function(x) x[[1]])), fun=function(x) median(x, na.rm=TRUE))
      
      
      
      ### Plotting ###
      
      #Obs, plot each dataset separately
      if (proj[p] == "Observed") {
        
        for (o in 1:length(metric_data)) {
          
          #Mask oceans, and crop so plot y-limits work better
          plot_data <- crop(mask(metric_data[[o]][[1]], world), extent(c(-180, 180, -65, 90)))
          
          
          #Plot map of obs trend
          plot(plot_data, breaks=lims[[vars[v]]] * 10, col=cols(length(lims[[vars[v]]]) -1),
               main="", ylim=c(-65, 90), legend=FALSE, bty="n", yaxt="n", xaxt="n")
          
          #Add world outline
          plot(world, add=TRUE, lwd=0.5)
          
          #Main title
          mtext(side=3, models[o], line=0)
          
          add_raster_legend2(cols=cols(length(lims[[metrics[m]]]) -1), 
                             limits=lims[[metrics[m]]][2:(length(lims[[metrics[m]]])-1)] * 10,
                             main_title="", plot_loc=c(0.12,0.88,0.07,0.10),
                             spt.cex=1)
          
          
          ### Add stippling ###
          
          #Stipple when trend significant
          pval <- mask(brick(metric_data[[o]][[2]]), world)
          
          #Find pixels where lag-1 autocore greated than CI
          ind    <- which(values(pval) <= 0.05)
          coords <- coordinates(pval)
          
          points(coords[ind,1], coords[ind,2], pch=20, lwd=0.2, cex=0.2)
          
          
        }
        
        #Models (plot median)
      } else {
        
        plot_lim <- lims[[vars[v]]]
        
        #Mask oceans, and crop so plot y-limits work better
        plot_data <- crop(mask(median_trend, world), extent(c(-180, 180, -65, 90)))
        
        
        #Plot map of median trend
        plot(plot_data, breaks=plot_lim, col=cols(length(plot_lim) -1),
             main="", ylim=c(-65, 90), legend=FALSE, bty="n", yaxt="n", xaxt="n")
        
        #Add world outline
        plot(world, add=TRUE, lwd=0.5)
        
        #Main title
        mtext(side=3, paste0(proj[p], " (n = ", length(models), ")"), line=0)
        
        add_raster_legend2(cols=cols(length(plot_lim) -1), 
                           limits=plot_lim[2:(length(plot_lim)-1)],
                           main_title="", plot_loc=c(0.12,0.88,0.07,0.10),
                           spt.cex=1)
        
        
        ### Add stippling ###
        
        #Stipple if at least 75% of models agree on median sign
        all_mod_data <- mask(brick(lapply(var_data, function(x) x[[1]])), world)
        
        #Calculate agreement
        perc_agreement <- calc(all_mod_data, fun=perc_agree_on_sign)
        
        #Find pixels where lag-1 autocore greated than CI
        ind    <- which(values(perc_agreement) >= agr_level)
        coords <- coordinates(perc_agreement)
        
        points(coords[ind,1], coords[ind,2], pch=20, lwd=0.2, cex=0.2 * 0.5)
        
      }
      
    } #project
    
    
    dev.off()
    

} #variables

