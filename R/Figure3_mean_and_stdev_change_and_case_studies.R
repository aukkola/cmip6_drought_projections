library(raster)
library(RColorBrewer)
library(maptools)
library(grDevices)
library(zoo)

#clear R environment
rm(list=ls(all=TRUE))


#Set path
path <- "/g/data/w35/amu561/CMIP6_drought/" #"/srv/ccrc/data04/z3509830/CMIP6_drought//"


#Source functions
source(paste0(path,"/scripts/R/functions/perc_agree_on_sign_weighted_AR4_method.R"))
source(paste0(path,"/scripts/R/functions/perc_agree_on_mean_weighted.R"))
source(paste0(path,"/scripts/R/functions/perc_agree_with_obs_mean_weighted.R"))
source(paste0(path,"/scripts/R/functions/add_raster_legend.R"))
source(paste0(path,"/scripts/R/functions/wtd_quantile.R"))
source(paste0(path,"/scripts/R/functions/wtd_ttest.R"))
source(paste0(path,"/scripts/R/functions/wtd_stdev.R"))



#Load world shapefile
world <- readShapePoly(paste0(path, "../World_shapefile/World"))


#Variables
vars <- c("pr")#, "mrro") #list.files(paste0(dr_path, exp[1]))


#List metrics
metrics <- c("mean", "stdev")
unit <- c("mm/month", "mm/month")

proj <- c("CMIP6")#, "CMIP5")

exp <- list(c("ssp585"))#,
           # c("rcp85"))


#Raster 1x1 deg for resampling to common resolution
extent_raster  <- raster(nrows=180, ncols=360, xmn=-180, xmx=180, ymn=-90, ymx=90)
stipple_raster <- raster(nrows=180/2, ncols=360/2, xmn=-180, xmx=180, ymn=-90, ymx=90)



#Set regions
regions <- list(Amazon=c(-78, -48, -19, 7),
                SouthAfrica = c(10, 40, -38, -15),
                #NorthernRussia =c(80, 100, 60, 75),
                Mediterranean = c(-12, 40, 28, 44),
                Australia = c(110, 155, -45, -20),
                Europe=c(-12, 30, 44, 62))




#####################
### Plot settings ###
#####################


#Set plot colours

# cols_diff <- colorRampPalette(c("#8c510a", "#d8b365", "#f6e8c3", "#f5f5f5",
#                "#c7eae5", "#5ab4ac", "#01665e"))

#Future difference
#Future difference
col_opts <- brewer.pal(11, 'RdYlBu')
col_opts[6] <- "grey80"
cols_diff <- colorRampPalette(col_opts) 


#Set output directory
outdir <- paste0(path, "/Figures/Paper_figures/")
dir.create(outdir, recursive=TRUE)



lims_diff <- list(mean  = c(-1000,  -35, -30, -25, -20, -15, -10, -5, -2.5,
                              2.5, 5, 10, 15, 20, 25, 30, 35,  1000),
                  stdev = c(-1000,  -35, -30, -25, -20, -15, -10, -5, -2.5,
                            2.5, 5, 10, 15, 20, 25, 30, 35,  1000))
                    
                    # c(-10000, -30, -25, -20, -15, -10, -7.5, -5, -2.5, 
                    #         2.5, 5, 7.5, 10, 15, 20, 25, 30, 10000))


#Level of model agreement for stippling (as fraction of models)
#agr_level <- 0.75


#Stippling settings
lwd <- 0.1
cex <- 0.15

panel_cex=0.7

#Initialise
land_area <- list()




##############################
### Mean and st. dev. maps ###
##############################

#Loop through variables
for (v in 1:length(vars)) {
  
  png(paste0(outdir, "/Figure3_Mean_changes_in_", vars[v], "_mean_and_stdev",
             "_CMIP6_CMIP5_Obs.png"),
      height=7.5, width=8.27, units="in", res=400)
  
  
  par(mai=c(0.3, 0.1, 0.3, 0.1))
  par(omi=c(0.1, 0.1, 0, 0.1))
  par(bty="n")
  
  layout(matrix(c(1,1,1,2,2,2, 3,3,4,4,5,5, 6,6,7,7,8,8), nrow=3, byrow=TRUE),
         heights=c(1.2, 0.8, 0.8))

  
  #Loop through metrics
  for (m in 1:length(metrics)) {
  
    
    ### Get observed data ###
    
    #Find files
    obs_files <- list.files(paste(path, "/Mean_and_variability_change/Observed/", 
                                  "/observed/", vars[v], sep="/"), recursive=TRUE, pattern=metrics[m], full.names=TRUE) 
    
    
    #Load data
    obs_data <- brick(lapply(obs_files, function(x) resample(raster(x), extent_raster)))
    
    #Calculate obs mean
    obs_mean <- mean(obs_data)
    

    
    #Loop through projects
    for (p in 1:length(proj)) {
      
      
      #######################
      ### Load model data ###
      #######################
      
      models <- c("BCC-CSM2-MR", "CESM2-WACCM", "CNRM-CM6-1",
                  "CanESM5", "GFDL-CM4", "IPSL-CM6A-LR", "MIROC6",
                  "MRI-ESM2-0", "UKESM1-0-LL")
      
      
      #Get historical data
      mod_files_hist <- list.files(paste(path, "/Mean_and_variability_change/", proj[p], 
                                         "/historical/", vars[v], sep="/"), recursive=TRUE,
                                   pattern=metrics[m], full.names=TRUE) 
      
      #Extract models
      mod_files_hist <- unlist(sapply(models, function(x) mod_files_hist[which(grepl(x, mod_files_hist))]))
      
      
      
      #Get RCP 8.5 data
      mod_files_rcp85 <- list.files(paste(path, "/Mean_and_variability_change/", proj[p], 
                                          exp[[p]], vars[v], sep="/"), recursive=TRUE,
                                    pattern=metrics[m], full.names=TRUE) 
      
      #Extract models
      mod_files_rcp85 <- unlist(sapply(models, function(x) mod_files_rcp85[which(grepl(x, mod_files_rcp85))]))
      
      
      #Load data
      mod_data_hist <- brick(lapply(mod_files_hist, function(x) resample(raster(x, stopIfNotEqualSpaced=FALSE), extent_raster)))
      
     
      
      #RCP8.5
      ind_early_85 <- which(grepl("_2015_2050_", mod_files_rcp85))
      ind_late_85  <- which(grepl("_2051_2100_", mod_files_rcp85)) 
  
      
      mod_data_rcp85_2100 <- brick(lapply(mod_files_rcp85[ind_late_85], function(x) resample(raster(x,stopIfNotEqualSpaced=FALSE), extent_raster))) #second period
      
      
      
      ##############################
      ### Work out model weights ###
      ##############################
      
      #Work these out separately for now in case mismatches in models
      
      #Historical
      models_hist <- sapply(mod_files_hist, function(x) strsplit(strsplit(x, paste0(metrics[m],"_pr_"))[[1]][2], "_")[[1]][1])
      unique_hist <- unique(models_hist)
      
      weights_hist <- vector(length=length(models_hist))
      
      for (u in 1:length(unique_hist)) { 
        ind <- which(models_hist == unique_hist[u])
        weights_hist[ind] <- 1 / length(ind)
      }
      
       
      
      #RCP8.5
      models_rcp85 <- sapply(mod_files_rcp85[ind_early_85], function(x) strsplit(strsplit(x, paste0(metrics[m],"_pr_"))[[1]][2], "_")[[1]][1])
      unique_rcp85 <- unique(models_rcp85)
      
      weights_rcp85 <- vector(length=length(models_rcp85))
      
      for (u in 1:length(unique_rcp85)) { 
        ind <- which(models_rcp85 == unique_rcp85[u])
        weights_rcp85[ind] <- 1/ length(which(models_rcp85 == unique_rcp85[u])) 
      }
      
      
      
      ################
      ### Plotting ###
      ################
 
      
      #--- Historical model mean (stippling where 75% of models within 10% of obs mean) ---#
      
      hist_plot_data <- weighted.mean(mod_data_hist, w=weights_hist, na.rm=TRUE)
      
      #Mask with obs
      hist_plot_data <- crop(mask(hist_plot_data, obs_mean), extent(c(-180, 180, -65, 85)))
      
 
      
      #--- Future mean difference (2015-2050), RCP 8.5 ---#
      
      fut_data <- crop(mod_data_rcp85_2100, extent(c(-180, 180, -65, 85)))
                       
      fut_plot_data <- weighted.mean(fut_data, w=weights_rcp85, na.rm=TRUE) - hist_plot_data
      
      
      #Plot
      # plot(fut_plot_data, breaks=lims_diff[[metrics[m]]], col=cols_diff(length(lims_diff[[metrics[m]]])-1),
      #      ylab="", xlab="", yaxt="n", xaxt="n", legend=FALSE)
      # 
      image(fut_plot_data, breaks=lims_diff[[metrics[m]]], col=cols_diff(length(lims_diff[[metrics[m]]])-1),
            axes=FALSE, ann=FALSE, asp=1, xlim=c(-180, 180), ylim=c(-65, 85))
      
      #World shapefile
      plot(world, border="grey50", add=TRUE, lwd=0.7)
      
      #Main title
      if (metrics[m] == "mean" ) {
        mtext(side=3, line=-3, text="Mean", xpd=NA)      #main title
        mtext(side=3, line=-3, adj=0, text="a", font=2, xpd=NA, cex=panel_cex) #panel number
        
      } else if (metrics[m] == "stdev") {
        mtext(side=3, line=-3, text="Standard deviation", xpd=NA)
        mtext(side=3, line=-3, adj=0, text="b", font=2, xpd=NA, cex=panel_cex) #panel number
        
      }
      
      
      #Legend
      len <- length(lims_diff[[metrics[m]]])-1
      
      add_raster_legend2(cols=cols_diff(len), limits=lims_diff[[metrics[m]]][2:len],
                         main_title=expression("mm"~"month"^"-1"), plot_loc=c(0.12,0.88,0.04, 0.07), 
                         title.cex=1, spt.cex=1, clip=TRUE, ysp_title_old=TRUE)
      
      
      #Stippling (at least 75% of models within 10% of obs)
      fut_mod_agr <- calc(fut_data - hist_plot_data, function(x) perc_agree_on_sign_weighted_AR4_method(x, weights=weights_rcp85))
      
      
      #Calculate land area where model changes are robust
      area_agree <- area(fut_mod_agr)
      area_agree[fut_mod_agr != 1 | is.na(fut_mod_agr)] <- NA
      
      area_total <- mask(area(fut_mod_agr), fut_mod_agr)
      
      land_area[[m]] <- sum(values(area_agree), na.rm=TRUE)  / 
                        sum(values(area_total), na.rm=TRUE)
      
      
      #Resample stippling to make it larger
      fut_mod_agr <- resample(fut_mod_agr, stipple_raster)
      
      #Find pixels where lag-1 autocore greated than CI
      ind    <- which(values(fut_mod_agr) ==1)
      coords <- coordinates(fut_mod_agr)
      
      #Add stippling
      points(coords[ind,1], coords[ind,2], pch=20, lwd=lwd, cex=cex)
      
      
      ### Add boxes for case study regions ###
      
      lapply(regions, function(x) polygon(c(x[1:2], rev(x[1:2])), c(x[3], x[3], x[4], x[4])))
      
      
      
      
    } #projects
    
  } #metrics
  
  
  
  
  
  ###################
  ### Time series ###
  ###################
   
  #Reset par
  par(mai=c(0.3, 0.45, 0.3, 0.1))
  #par(mai=c(0.35, 0.45, 0.5, 0.1))
  
  
  #Set regions
  regions <- c("Mediterranean", "Europe", "Amazon",  
               "Mediterranean", "Europe", "Amazon")
  
  reg_labs <- c("Mediterranean", "Central Europe", "Amazon")
  
  fun <- c("mean", "mean", "mean",
           "sd", "sd", "sd")
  
  ylab <- c(expression("Precipitation"~"(mm"~"month"^"-1"*")"),
            expression("St. deviation"~"(mm"~"month"^"-1"*")"))
  
  panel_lab <- c("c", "d", "e",
                 "f", "g", "h")
  
  
  #ylims <- list(c(15, 50), c(55, 95), c(50, 230), c(10, 22), c(15, 35), c(27, 90))
  
  
  subset_models <- c("BCC-CSM2-MR", "CESM2-WACCM", "CNRM-CM6-1",
                     "CanESM5", "GFDL-CM4", "IPSL-CM6A-LR", "MIROC6",
                     "MRI-ESM2-0", "UKESM1-0-LL")
  
  
  for (r in 1:length(regions)){
   
    
    file45 <- paste0(path, "/Timeseries_mean_var/CMIP6/ssp245/pr/Timeseries_mean_pr_CMIP6_ssp245_", 
                     regions[r], ".csv")
    file85 <- paste0(path, "/Timeseries_mean_var/CMIP6/ssp585/pr/Timeseries_mean_pr_CMIP6_ssp585_",
                     regions[r], ".csv")
    
    obs_file <- paste0(path, "/Timeseries_mean_var/Observed/observed/pr/Timeseries_mean_pr_Observed_observed_",
                       regions[r], ".csv")
    
    #Load data
    data45 <- read.csv(file45, header=TRUE)
    data85 <- read.csv(file85, header=TRUE)
    
    obs_data <- read.csv(obs_file, header=TRUE)
    
    
    #Remove extra models
    mod_inds <- which(data45[,1] %in% subset_models)
    
    data45 <- data45[mod_inds, ]
    data85 <- data85[mod_inds, ]
    

    #Work out weights
    models <- data45[,1]
    
    unique_mods <- unique(models)
    
    weights <- vector(length=length(models))
    
    for (u in 1:length(unique_mods)) { 
      ind <- which(models == unique_mods[u])
      weights[ind] <- 1 / length(ind)
    }
    
    
    
    #Remove model column
    data45 <- data45[,2:ncol(data45)]
    data85 <- data85[,2:ncol(data85)]
    
    obs_data <- obs_data[,2:ncol(obs_data)]
    
    
    #Calculate rolling means
    if (fun[r] == "mean") {
      
      rollmean45   <- apply(data45, MARGIN=1, rollmean, k=24)
      rollmean85   <- apply(data85, MARGIN=1, rollmean, k=24)
      rollmean_obs <- apply(obs_data, MARGIN=1, rollmean, k=24)
      
      
    } else if (fun[r] == "sd"){
      
      rollmean45   <- apply(data45, MARGIN=1, function(x) rollapply(x, FUN=sd, width=120))
      rollmean85   <- apply(data85, MARGIN=1, function(x) rollapply(x, FUN=sd, width=120))
      rollmean_obs <- apply(obs_data, MARGIN=1, function(x) rollapply(x, FUN=sd, width=120))
      
    }
    
    
    
    #Ensemble mean
    mean_45 <- apply(rollmean45, MARGIN=1, weighted.mean, w=weights)
    mean_85 <- apply(rollmean85, MARGIN=1, weighted.mean, w=weights)

    mean_obs <- apply(rollmean_obs, MARGIN=1, mean, na.rm=TRUE)
    
    
    #Extract historical period
    hist_end <- which(colnames(data45) == "X2015") #which(rownames(rollmean45) == "X2015")
    
    #Need a different historical end time for mean and st dev 
    #because of different smoothing window
    if(r < 4) {
      hist_end <- which(colnames(data45) == "X2013")
    } else {
      hist_end <- which(colnames(data45) == "X2005")
    }
    
    
    
    hist_range  <- apply(rollmean45[1:hist_end,], MARGIN=1, wtd_quantile, weights=weights, probs=c(0, 1))
    
    rcp45_range <- apply(rollmean45[(hist_end + 1) : nrow(rollmean45),], MARGIN=1, 
                         wtd_quantile, weights=weights, probs=c(0, 1))
    
    rcp85_range <- apply(rollmean85[(hist_end + 1) : nrow(rollmean85),], MARGIN=1, 
                         wtd_quantile, weights=weights, probs=c(0, 1))
    
    
    #Set time vectors
    all_time <- seq.Date(as.Date("1900/1/1"), by="months", length.out=length(mean_45))
    
    #Save Amazon time so can match x-axes
    if(r==1) all_time_Am <- all_time
    
    
    #Initialise plot
    plot(range(all_time_Am), range(c(hist_range, rcp45_range, rcp85_range, range(mean_obs))), type="n", 
         xlab="", ylab="", mgp=c(3, .5, 0), bty='L')
    
    #Plot historical range
    polygon(c(all_time[1:hist_end], all_time[hist_end:1]), 
            c(hist_range[1,], rev(hist_range[2,])),
            col=adjustcolor("grey50", alpha.f=0.4), border=NA)
    
    #Plot observations
    #lapply(1:ncol(rollmean_obs), function(x) lines(all_time[1:length(rollmean_obs[,x])], rollmean_obs[,x], lty=2))
    lines(all_time[1:length(mean_obs)], mean_obs, lwd=0.5, col="grey10")
    
    
    #Plot historical mean
    lines(all_time[1:hist_end], mean_45[1:hist_end], col="grey50")
    
    
    
    #Start index for future period    
    fut_ind <- hist_end + 1
    
    #Plot futrure ranges
    polygon(c(all_time[fut_ind:length(all_time)], all_time[length(all_time):fut_ind]), 
            c(rcp45_range[1,], rev(rcp45_range[2,])),
            col=adjustcolor(col_opts[length(col_opts)-1], alpha.f=0.4), border=NA)
    
    
    polygon(c(all_time[fut_ind:length(all_time)], all_time[length(all_time):fut_ind]), 
            c(rcp85_range[1,], rev(rcp85_range[2,])),
            col=adjustcolor(col_opts[2], alpha.f=0.4), border=NA)
    
    
    #Plot future means    
    lines(all_time[fut_ind:length(all_time)], mean_45[fut_ind:length(mean_45)],
          col=col_opts[length(col_opts)-1])
    
    lines(all_time[fut_ind:length(all_time)], mean_85[fut_ind:length(mean_85)],
          col=col_opts[2])
    
    
    #Labels
    mtext(side=2, line=2, padj=-10.2, text=panel_lab[r], font=2, xpd=NA, cex=panel_cex, las=2) #panel number
    
    #y-label
    if (r == 1) mtext(side=2, line=2, text=ylab[1], xpd=NA, cex=panel_cex) #precip
    if (r == 4) mtext(side=2, line=2, text=ylab[2], xpd=NA, cex=panel_cex) #st dev
    
    
    #x-label
    if (r == 5) mtext(side=1, line=2, text="Year", xpd=NA, cex=panel_cex) 
    
    #Region label
    if(r < 4) mtext(side=3, line=1, text=reg_labs[r], xpd=NA, cex=panel_cex) 
    
    
    #legend (split in two to align text better)
    if ( r == 1) {
      
      legend(x=as.Date("1895-01-01"), y=23, legend=c("Observed", "SSP2-4.5"), bty="n",
             col=c("grey10", col_opts[length(col_opts)-1]), lty=1, lwd=2, cex=0.8, seg.len=1)
      
      legend(x=as.Date("1960-01-01"), y=23, legend=c("Historical", "SSP5-8.5"), bty="n",
             col=c("grey50", col_opts[2]), lty=1, lwd=2, cex=0.8, seg.len=1)
      
      
    }
    
    #Add obs
    
    
    
    
    ### Add change in mean and sd ###
    
    #Set time vectors
    hist_start <- which(all_time == "1950-01-01")
    hist_end   <- which(all_time == "2014-12-01")
    
    fut_start  <- which(all_time == "2050-01-01")
    fut_end    <- which(all_time == "2090-01-01")
    
    
    #Calculate mean and sd dev
    mean_hist <- rowMeans(data85[,hist_start:hist_end])
    mean_fut  <- rowMeans(data85[,fut_start:fut_end])
    
    sd_hist <- apply(data85[,hist_start:hist_end], MARGIN=1, FUN=sd)
    sd_fut  <- apply(data85[,fut_start:fut_end], MARGIN=1, FUN=sd)
    
    #Weighted t-test for change in mean and standard deviation    
    ttest_mean <- wtd.t.test(x=mean_hist, y=mean_fut, weight=weights)
    ttest_sd   <- wtd.t.test(x=sd_hist, y=sd_fut, weight=weights)
    
    #Calculate future change
    d_mean <- ttest_mean$additional["Mean.y"] - ttest_mean$additional["Mean.x"]
    d_sd <- ttest_sd$additional["Mean.y"] - ttest_sd$additional["Mean.x"]
    

    pval_mean <- round(ttest_mean$coefficients[3], digits=3)
    pval_sd   <- round(ttest_sd$coefficients[3], digits=3)
    
    
    # #Add to figure    
    # mtext(side=3, adj=0,  bquote("mean ="~.(round(d_mean, digits=2))~mm~m^{-1}~"(p ="~.(sprintf("%.3f", pval_mean))*')'),
    #       line=-1, cex=0.5)
    # 
    # mtext(side=3, adj=0,  bquote("sd ="~.(round(d_sd, digits=2))~mm~m^{-1}~"(p ="~.(sprintf("%.3f", pval_sd))*')'),
    #       line=-3, cex=0.5)
    
  }
  
  
  
  dev.off()
  
} #variables

  
  





  

#   
#   
#   
#   
#   ### Amazon ###
#   
#   #Show change in mean rainfall under RCP4.5 nad 8.5
#   #Also print change in standard deviation with intervals from t-test
#   
#   
#   
#   ##############  
#   ### Europe ###
#   ##############
#   
#   
#   k=120
#   
#   #Save Amazon time to match x-axis
#   all_time_Am <- all_time
#   
#   
#   #Show change in mean rainfall under RCP4.5 nad 8.5
#   #Also print change in standard deviation with intervals from t-test
#   
#   file45 <- paste0(path, "/Timeseries_mean_var/CMIP6/ssp245/pr/Timeseries_mean_pr_CMIP6_ssp245_Europe.csv")
#   file85 <- paste0(path, "/Timeseries_mean_var/CMIP6/ssp585/pr/Timeseries_mean_pr_CMIP6_ssp585_Europe.csv")
#   
#   #Load data
#   data45 <- read.csv(file45, header=TRUE)
#   data85 <- read.csv(file85, header=TRUE)
#   
#   
#   #Remove model column
#   data45 <- data45[,2:ncol(data45)]
#   data85 <- data85[,2:ncol(data85)]
#   
#   #Calculate rolling means
#   rollmean45 <- apply(data45, MARGIN=1, function(x) rollapply(x, FUN=sd, width=k))
#   rollmean85 <- apply(data85, MARGIN=1, function(x) rollapply(x, FUN=sd, width=k))
#   
#   
#   mean_45 <- apply(rollmean45, MARGIN=1, weighted.mean, w=weights)
#   mean_85 <- apply(rollmean85, MARGIN=1, weighted.mean, w=weights)
#   
# 
#   #Extract historical period
#   hist_end <- which(colnames(data45) == "X2015") #can't use rollmean45, names not preserved but should not matter
#   
#   
#   rcp45_range <- apply(rollmean45, MARGIN=1, 
#                        wtd_quantile, weights=weights, probs=c(0.25, 0.75))
#   
#   rcp85_range <- apply(rollmean85, MARGIN=1, 
#                        wtd_quantile, weights=weights, probs=c(0.25, 0.75))
#   
#   
#   #Set time vectors
#   all_time <- seq.Date(as.Date("1900/1/1"), by="months", length.out=length(mean_45))
#   
#   #Initialise plot
#   plot(range(all_time_Am), range(c(rcp45_range, rcp85_range)), type="n", 
#        xlab="", ylab="", mgp=c(3, .5, 0), bty='L')
#   
#   #Plot historical
#   lines(all_time[1:hist_end], mean_45[1:hist_end], col="grey50")
#   
#   polygon(c(all_time[1:hist_end], all_time[hist_end:1]), 
#           c(rcp45_range[1, 1:hist_end], rcp45_range[2, hist_end:1]),
#           col=adjustcolor("grey50", alpha.f=0.4), border=NA)
#   
#   
#   #Plot RCP4.5
#   fut_ind <- hist_end + 1
#   end_ind <- length(all_time)
#   
#   polygon(c(all_time[fut_ind:end_ind], all_time[end_ind:fut_ind]), 
#           c(rcp45_range[1, fut_ind:end_ind], rcp45_range[2, end_ind:fut_ind]),
#           col=adjustcolor(col_opts[length(col_opts)-1], alpha.f=0.4), border=NA)
#   
#   
#   polygon(c(all_time[fut_ind:end_ind], all_time[end_ind:fut_ind]), 
#           c(rcp85_range[1, fut_ind:end_ind], rcp85_range[2, end_ind:fut_ind]),
#           col=adjustcolor(col_opts[2], alpha.f=0.4), border=NA)
#   
#   
#   #Plot RCP8.5
#   
#   lines(all_time[fut_ind:length(all_time)], mean_45[fut_ind:length(mean_45)],
#         col=col_opts[length(col_opts)-1])
#   
#   lines(all_time[fut_ind:length(all_time)], mean_85[fut_ind:length(mean_85)],
#         col=col_opts[2])
#   
#   
#   #Labels
#   mtext(side=2, line=2, padj=-10.2, text="d", font=2, xpd=NA, cex=panel_cex, las=2) #panel number
#   mtext(side=2, line=2, text="St. deviation (mm/month)", xpd=NA, cex=panel_cex) #y-label
#   mtext(side=1, line=2, text="Year", xpd=NA, cex=panel_cex) #x-label
#   mtext(side=3, line=-1, text="Europe", xpd=NA, cex=panel_cex) #x-label
#   
#   
#   
#   
#   #################
#   ### Australia ###
#   #################
#   
#   #Smoothing factor
#   k=24
#   
#   region <- "Australia"
#   
#   #Files
#   file45 <- paste0(path, "/Timeseries_mean_var/CMIP6/ssp245/pr/Timeseries_mean_pr_CMIP6_ssp245_", 
#                    region, ".csv")
#   file85 <- paste0(path, "/Timeseries_mean_var/CMIP6/ssp585/pr/Timeseries_mean_pr_CMIP6_ssp585_", 
#                    region, ".csv")
#   
#   #Load data
#   data45 <- read.csv(file45, header=TRUE)
#   data85 <- read.csv(file85, header=TRUE)
#   
#   
#   #Remove model column
#   data45 <- data45[,2:ncol(data45)]
#   data85 <- data85[,2:ncol(data85)]
#   
#   
#   #Work out weights
#   models <- data45[,1]
#   
#   unique_mods <- unique(models)
#   
#   weights <- vector(length=length(models))
#   
#   for (u in 1:length(unique_mods)) { 
#     ind <- which(models == unique_mods[u])
#     weights[ind] <- 1 / length(ind)
#   }
#   
#   
#   
#   #Remove model column
#   data45 <- data45[,2:ncol(data45)]
#   data85 <- data85[,2:ncol(data85)]
#   
#   #Calculate rolling means
#   rollmean45 <- apply(data45, MARGIN=1, rollmean, k=k)
#   rollmean85 <- apply(data85, MARGIN=1, rollmean, k=k)
#   
#   
#   mean_45 <- apply(rollmean45, MARGIN=1, weighted.mean, w=weights)
#   mean_85 <- apply(rollmean85, MARGIN=1, weighted.mean, w=weights)
#   
#   
#   
#   #Extract historical period
#   hist_end <- which(rownames(rollmean45) == "X2015")
#   
#   
#   hist_range  <- apply(rollmean45[1:hist_end,], MARGIN=1, wtd_quantile, weights=weights, probs=c(0.25, 0.75))
#   
#   rcp45_range <- apply(rollmean45[(hist_end + 1) : nrow(rollmean45),], MARGIN=1, 
#                        wtd_quantile, weights=weights, probs=c(0.25, 0.75))
#   
#   rcp85_range <- apply(rollmean85[(hist_end + 1) : nrow(rollmean85),], MARGIN=1, 
#                        wtd_quantile, weights=weights, probs=c(0.25, 0.75))
#   
#   
#   #Set time vectors
#   all_time <- seq.Date(as.Date("1900/1/1"), by="months", length.out=length(mean_45))
#   
#   #Initialise plot
#   plot(range(all_time), range(c(hist_range, rcp45_range, rcp85_range)), type="n", 
#        xlab="", ylab="", mgp=c(3, .5, 0), bty='L')
#   
#   #Plot historical
#   lines(all_time[1:hist_end], mean_45[1:hist_end], col="grey50")
#   
#   polygon(c(all_time[1:hist_end], all_time[hist_end:1]), 
#           c(hist_range[1,], rev(hist_range[2,])),
#           col=adjustcolor("grey50", alpha.f=0.4), border=NA)
#   
#   
#   #Plot RCP4.5
#   fut_ind <- hist_end + 1
#   
#   
#   polygon(c(all_time[fut_ind:length(all_time)], all_time[length(all_time):fut_ind]), 
#           c(rcp45_range[1,], rev(rcp45_range[2,])),
#           col=adjustcolor(col_opts[length(col_opts)-1], alpha.f=0.4), border=NA)
#   
#   
#   polygon(c(all_time[fut_ind:length(all_time)], all_time[length(all_time):fut_ind]), 
#           c(rcp85_range[1,], rev(rcp85_range[2,])),
#           col=adjustcolor(col_opts[2], alpha.f=0.4), border=NA)
#   
#   
#   #Plot RCP8.5
#   
#   lines(all_time[fut_ind:length(all_time)], mean_45[fut_ind:length(mean_45)],
#         col=col_opts[length(col_opts)-1])
#   
#   lines(all_time[fut_ind:length(all_time)], mean_85[fut_ind:length(mean_85)],
#         col=col_opts[2])
#   
#   
#   #Labels
#   mtext(side=2, line=2, padj=-10.2, text="e", font=2, xpd=NA, cex=panel_cex, las=2) #panel number
#   mtext(side=2, line=2, text="Precipitation (mm/month)", xpd=NA, cex=panel_cex) #y-label
#   mtext(side=1, line=2, text="Year", xpd=NA, cex=panel_cex) #x-label
#   mtext(side=3, line=-1, text=region, xpd=NA, cex=panel_cex) #x-label
#   
#   # #legend (split in two to align text better)
#   # legend(x=as.Date("1895-01-01"), y=125, legend=c("Observed", "SSP2-4.5"), bty="n",
#   #        col=c("black", col_opts[length(col_opts)-1]), lty=1, lwd=2, cex=0.8, seg.len=1)
#   # 
#   # legend(x=as.Date("1960-01-01"), y=125, legend=c("Historical", "SSP5-8.5"), bty="n",
#   #        col=c("grey50", col_opts[2]), lty=1, lwd=2, cex=0.8, seg.len=1)
#   
#   
#   #Add obs
#   
#   
#   
#   
#   
#   dev.off()
#   
#   
# } #variables
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 

subset_models <- c("BCC-CSM2-MR", "CESM2-WACCM", "CNRM-CM6-1",
                   "CanESM5", "GFDL-CM4", "IPSL-CM6A-LR", "MIROC6",
                   "MRI-ESM2-0", "UKESM1-0-LL")


path <- "~/Documents/CMIP6_drought_projections//"
region = "Australia"

#Show change in mean rainfall under RCP4.5 nad 8.5
#Also print change in standard deviation with intervals from t-test

file45 <- paste0(path, "/Timeseries_mean_var/CMIP6/ssp245/pr/Timeseries_mean_pr_CMIP6_ssp245_", region, ".csv")
file85 <- paste0(path, "/Timeseries_mean_var/CMIP6/ssp585/pr/Timeseries_mean_pr_CMIP6_ssp585_", region, ".csv")

#Load data
data45 <- read.csv(file45, header=TRUE)
data85 <- read.csv(file85, header=TRUE)


#Remove extra models
mod_inds <- which(data45[,1] %in% subset_models)

data45 <- data45[mod_inds, ]
data85 <- data85[mod_inds, ]


#Work out weights
models <- data85[,1]

unique_mods <- unique(models)

weights <- vector(length=length(models))

for (u in 1:length(unique_mods)) {
  ind <- which(models == unique_mods[u])
  weights[ind] <- 1 / length(ind)
}


#Remove model column
data45 <- data45[,2:ncol(data45)]
data85 <- data85[,2:ncol(data85)]

# #Calculate rolling means: stdev
# rollsd45 <- apply(data45, MARGIN=1, function(x) rollapply(x, FUN=sd, width=k))
# rollsd85 <- apply(data85, MARGIN=1, function(x) rollapply(x, FUN=sd, width=k))
#
#
# #Calculate rolling means: mean
# rollmean45 <- apply(data45, MARGIN=1, function(x) rollapply(x, FUN=mean, width=k))
# rollmean85 <- apply(data85, MARGIN=1, function(x) rollapply(x, FUN=mean, width=k))
#



#Set time vectors
all_time <- seq.Date(as.Date("1900/1/1"), by="months", length.out=ncol(data45))

hist_start <- which(all_time == "1950-01-01")
hist_end   <- which(all_time == "2014-12-01")

fut_start  <- which(all_time == "2050-01-01")
fut_end    <- which(all_time == "2090-01-01")


#Calculate mean and sd dev
mean_hist <- rowMeans(data85[,hist_start:hist_end])
mean_fut  <- rowMeans(data85[,fut_start:fut_end])

sd_hist <- apply(data85[,hist_start:hist_end], MARGIN=1, FUN=sd)
sd_fut  <- apply(data85[,fut_start:fut_end], MARGIN=1, FUN=sd)


library(weights)
ttest_mean <- wtd.t.test(x=mean_hist, y=mean_fut, weight=weights)
ttest_sd   <- wtd.t.test(x=sd_hist, y=sd_fut, weight=weights)


### Drought indices ###

file_dur <-  paste0(path, "/Timeseries/CMIP6/ssp245/pr/Perc_15/Baseline_1950_2014/Scale_3/duration/Timeseries_pr_CMIP6_ssp245_duration_", region, ".csv")
file_int <-  paste0(path, "/Timeseries/CMIP6/ssp245/pr/Perc_15/Baseline_1950_2014/Scale_3/intensity/Timeseries_pr_CMIP6_ssp245_intensity_", region, ".csv")


#Load data
data_dur <- read.csv(file_dur, header=TRUE)
data_int <- read.csv(file_int, header=TRUE)

#Work out weights
models <- data_dur[,1]

unique_mods <- unique(models)

weights <- vector(length=length(models))

for (u in 1:length(unique_mods)) {
  ind <- which(models == unique_mods[u])
  weights[ind] <- 1 / length(ind)
}


#Remove model column
data_dur <- data_dur[,2:ncol(data_dur)]
data_int <- data_int[,2:ncol(data_int)]

#Means
mean_hist_dur <- rowMeans(data_dur[,hist_start:hist_end], na.rm=TRUE)
mean_fut_dur  <- rowMeans(data_dur[,fut_start:fut_end], na.rm=TRUE)

mean_hist_int <- rowMeans(data_int[,hist_start:hist_end], na.rm=TRUE)
mean_fut_int  <- rowMeans(data_int[,fut_start:fut_end], na.rm=TRUE)

#t-test
ttest_dur <- wtd.t.test(x=mean_hist_dur, y=mean_fut_dur, weight=weights)
ttest_int   <- wtd.t.test(x=mean_hist_int, y=mean_fut_int, weight=weights)


