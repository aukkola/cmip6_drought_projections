library(raster)
library(RColorBrewer)
library(maptools)


#clear R environment
rm(list=ls(all=TRUE))


#Set path
path <- "/g/data/w35/amu561/CMIP6_drought/" #"/srv/ccrc/data04/z3509830/CMIP6_drought//"


#Source functions
source(paste0(path,"/scripts/R/functions/perc_agree_on_sign_weighted_AR4_method.R"))
source(paste0(path,"/scripts/R/functions/perc_agree_on_mean_weighted.R"))
source(paste0(path,"/scripts/R/functions/perc_agree_with_obs_mean_weighted.R"))
source(paste0(path,"/scripts/R/functions/wtd_stdev.R"))
source(paste0(path,"/scripts/R/functions/add_raster_legend.R"))


#Load world shapefile
world <- readShapePoly(paste0(path, "../World_shapefile/World"))





#Set percentile and scale
percentile <- "Perc_15"

scale      <- "Scale_12"

baseline   <- "Baseline_1950_2014"


#Variables
vars <- c("pr")#, "mrro") #list.files(paste0(dr_path, exp[1]))


#List metrics
metrics <- c("duration", "intensity", "frequency")

proj <- c("CMIP6", "CMIP5")

exp <- list(c("ssp585"),
            c("rcp85"))


#Raster 1x1 deg for resampling to common resolution
extent_raster  <- raster(nrows=180, ncols=360, xmn=-180, xmx=180, ymn=-90, ymx=90)
stipple_raster <- raster(nrows=180/2, ncols=360/2, xmn=-180, xmx=180, ymn=-90, ymx=90)

#####################
### Plot settings ###
#####################


#Set plot colours

#Historical mean
cols_hist <- colorRampPalette(c("#ffffe5", "#fee391",
                                "#fe9929", "#cc4c02"))


#Future difference
col_opts <- rev(brewer.pal(11, 'RdYlBu'))
col_opts[6] <- "grey80"
cols_diff <- colorRampPalette(col_opts) 

#Set output directory
outdir <- paste0(path, "/Figures/Paper_figures/")
dir.create(outdir, recursive=TRUE)


lims_hist <- list(duration  = c(1, 4, 4.3, 4.6, 4.9, 5.2, 5.5, 5.8, 1000),
                  intensity = c(0, 10, 15, 20, 25, 30, 40, 50, 60, 10000),
                  #rel_intensity = c(0, 10, 15, 20, 25, 30, 35, 40, 100),
                  frequency = c(0,  3.2, 3.4, 3.6, 3.8, 4.0, 4.2, 4.4, 1000))

lims_diff <- list(duration      = c(-1000, -6, -5, -4, -3, -2.0, -1.5, -1, 1, 1.5, 2.0, 3, 4, 5, 6, 1000),
                  intensity     = c(-10000, -40, -35, -30, -25, -20, -15, -10, -5, 5, 10, 15, 20, 25,  30, 35, 40, 10000),
                  #rel_intensity = c(-100, -40, -30, -20, -10, 10, 20, 30, 40, 100),
                  frequency     = c(-100, -4, -3.5, -3, -2.5, -2, -1.5, -1, -0.5, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 100))


unit <- c("months", expression("mm"~"month"^"-1"), expression("no. events 10 yrs"^"-1"))

#Level of model agreement for stippling (as fraction of models)
agr_level <- 0.75


#Stippling settings
lwd <- 0.1
cex <- 0.15

panel_cex=0.7

#Initialise
land_area_hist <- list()
land_area      <- list()


#Loop through variables
for (v in 1:length(vars)) {
  
  
  #Loop through metrics
  for (m in 1:length(metrics)) {
    
    
    
    ### Set up figure ###
    png(paste0(outdir, "/FigureS", m+9, "_Mean_changes_in_", vars[v], "_", metrics[m], "_",
               percentile, "_", scale, "_CMIP6_CMIP5_Obs_Scale12.png"),
        height=5.5, width=9.7, units="in", res=400)
    
    
    par(mai=c(0, 0.1, 0, 0.1))
    par(omi=c(0.6, 0.3, 0.5, 0.1))
    
    par(mfrow=c(2, 2))
    par(bty="n")
    
    ### Get observed data ###
    
    #Find files
    obs_files <- list.files(paste(path, "/Drought_metrics/Mean_changes/Observed/", 
                                  "/observed/", vars[v], percentile, baseline,
                                  scale, metrics[m], sep="/"), full.names=TRUE) 
    
    
    #Load data
    obs_data <- brick(lapply(obs_files, raster))
    
    if (metrics[m] == "frequency") obs_data <- obs_data * 120
    
    
    #Calculate obs mean
    obs_mean <- mean(obs_data)
    
    #Calculate obs agreement
    obs_agr <- mask(calc(obs_data, perc_agree_on_mean_weighted, weights=NA), obs_mean)
    
    
    #Initialise
    land_area_hist[[m]] <- vector()
    land_area[[m]]      <- vector()
    
    
    #Loop through projects
    for (p in 1:length(proj)) {
      
      
      #######################
      ### Load model data ###
      #######################
      
      if (proj[p] == "CMIP6") {
        
        models <- c("BCC-CSM2-MR", "CESM2-WACCM", "CNRM-CM6-1",
                    "CanESM5", "GFDL-CM4", "IPSL-CM6A-LR", "MIROC6",
                    "MRI-ESM2-0", "UKESM1-0-LL")
        
        #"CAMS-CSM1-0"
        #"CESM2"
        #"CNRM-ESM2-1"
        #"EC-Earth3-Veg"
        #"MIROC-ES2L"
        #
        
      } else if (proj[p] == "CMIP5") {
        
        models <- c("bcc-csm1-1", "CESM1-WACCM", "CNRM-CM5", "CanESM2",
                    "GFDL-CM3", "IPSL-CM5A-LR", "MIROC5", "MRI-CGCM3",
                    "HadGEM2-ES")
      }
      
      #Get historical data
      mod_files_hist <- list.files(paste(path, "/Drought_metrics/Mean_changes/", proj[p], 
                                         "/historical/", vars[v], percentile, baseline,
                                         scale, metrics[m], sep="/"), full.names=TRUE) 
      
      #Extract models
      mod_files_hist <- unlist(sapply(models, function(x) mod_files_hist[which(grepl(x, mod_files_hist))]))
      
      
      #Get RCP 8.5 data
      mod_files_rcp85 <- list.files(paste(path, "/Drought_metrics/Mean_changes/", proj[p], 
                                          exp[[p]], vars[v], percentile, baseline,
                                          scale, metrics[m], sep="/"), full.names=TRUE) 
      
      #Extract models
      mod_files_rcp85 <- unlist(sapply(models, function(x) mod_files_rcp85[which(grepl(x, mod_files_rcp85))]))
      
      
      #Load data
      mod_data_hist <- brick(lapply(mod_files_hist, raster))
      
      #Only use later period for future
      mod_data_rcp85_2100 <- brick(lapply(mod_files_rcp85, function(x) brick(x)[[2]])) #second period
      
      
      
      ##############################
      ### Work out model weights ###
      ##############################
      
      #Work these out separately for now in case mismatches in models
      
      #Historical
      models_hist <- sapply(mod_files_hist, function(x) strsplit(strsplit(x, "Mean_changes_1950_2014_")[[1]][2], "_")[[1]][1])
      unique_hist <- unique(models_hist)
      
      weights_hist <- vector(length=length(models_hist))
      
      for (u in 1:length(unique_hist)) { 
        ind <- which(models_hist == unique_hist[u])
        weights_hist[ind] <- 1 / length(ind)
      }
      
      
      
      #RCP8.5
      models_rcp85 <- sapply(mod_files_rcp85, function(x) strsplit(strsplit(x, "Mean_changes_2015_2050_and_2051_2099_")[[1]][2], "_")[[1]][1])
      unique_rcp85 <- unique(models_rcp85)
      
      weights_rcp85 <- vector(length=length(models_rcp85))
      
      for (u in 1:length(unique_rcp85)) { 
        ind <- which(models_rcp85 == unique_rcp85[u])
        weights_rcp85[ind] <- 1/ length(which(models_rcp85 == unique_rcp85[u])) 
      }
      
      
      
      
      ################
      ### Plotting ###
      ################
      
      
      
      #--- Historical model mean (stippling where 75% of models within 10% of obs mean) ---#
      
      if (metrics[m] == "frequency") {
        mod_data_hist <- mod_data_hist * 120
      }
      
      
      hist_plot_data <- weighted.mean(mod_data_hist, w=weights_hist, na.rm=TRUE)
      
      #Mask with obs
      hist_plot_data <- crop(mask(hist_plot_data, obs_mean), extent(c(-180, 180, -65, 85)))
      
      #Plot
      image(hist_plot_data, breaks=lims_hist[[metrics[m]]], col=cols_hist(length(lims_hist[[metrics[m]]])-1),
            axes=FALSE, ann=FALSE, asp=1, xlim=c(-180, 180), ylim=c(-65, 85))
      
      
      #World outline
      plot(world, add=TRUE, border="grey50")
      
      
      #Main title
      mtext(side=2, line=1, text=proj[p], xpd=NA)
      
      #Time period label
      if(p==1) mtext(side=3, line=1, font=2, text=paste0("Historical mean (1950-2014)"), xpd=NA)
      
      
      #Legend
      len <- length(lims_hist[[metrics[m]]])-1
      
      if(p==2) add_raster_legend2(cols=cols_hist(len), limits=lims_hist[[metrics[m]]][2:len],
                                  main_title=unit[m], plot_loc=c(0.12,0.88,-0.17,-0.14), title.cex=1, spt.cex=1, clip=TRUE)
      
      
      #Stippling (at least 75% of models within 10% of obs)
      hist_mod_agr <- calc(brick(list(obs_mean, mod_data_hist)), function(x) 
                            perc_agree_with_obs_mean_weighted(x, weights=weights_hist))
      
      
      hist_mod_agr=resample(hist_mod_agr, stipple_raster)
      
      
      
      #Calculate land area where models agree with obs
      area_agree_hist <- area(hist_mod_agr)
      area_agree_hist[hist_mod_agr < agr_level | is.na(hist_mod_agr)] <- NA
      
      area_total <- mask(area(hist_mod_agr), hist_mod_agr)
      
      land_area_hist[[m]][p] <- sum(values(area_agree_hist), na.rm=TRUE)  / 
        sum(values(area_total), na.rm=TRUE)
 
      
      #Find pixels where lag-1 autocore greated than CI
      ind    <- which(values(hist_mod_agr) >= agr_level)
      coords <- coordinates(hist_mod_agr)
      
      #Add stippling
      points(coords[ind,1], coords[ind,2], pch=20, lwd=lwd, cex=cex)
      
      if (p ==1 ){
        mtext(side=3, line=0, adj=0, text="a", font=2, xpd=NA, cex=panel_cex) #panel number
        
      } else {
        mtext(side=3, line=0, adj=0, text="b", font=2, xpd=NA, cex=panel_cex) #panel number
        
      }
      
      
      
      #--- Future mean difference (2015-2050), RCP 8.5 ---#
      
      if (metrics[m] == "frequency") {
        mod_data_rcp85_2100 <- mod_data_rcp85_2100 * 120
      }
      
      
      fut_data <- crop(mod_data_rcp85_2100, extent(c(-180, 180, -65, 85)))
      
      fut_plot_data <- weighted.mean(fut_data, w=weights_rcp85, na.rm=TRUE) - hist_plot_data
      
      
      #Plot
      image(fut_plot_data, breaks=lims_diff[[metrics[m]]], col=cols_diff(length(lims_diff[[metrics[m]]])-1),
            axes=FALSE, ann=FALSE, asp=1, xlim=c(-180, 180), ylim=c(-65, 85))
      
    
      
      #World outline
      plot(world, add=TRUE, border="grey50", lwd=0.7)
      
      
      #Time period label
      if(p==1) mtext(side=3, line=1, font=2, text="Future change (2051-2100)", xpd=NA)
      
      
      #Legend
      len <- length(lims_diff[[metrics[m]]])-1
      if(p==2) add_raster_legend2(cols=cols_diff(len), limits=lims_diff[[metrics[m]]][2:len],
                                  main_title=unit[m], plot_loc=c(0.12,0.88,-0.17,-0.14), 
                                  title.cex=1, spt.cex=1, clip=TRUE, ysp_title_old=TRUE)
      
      
      #Stippling (at least 75% of models within 10% of obs)
      fut_mod_agr <- calc(fut_data - hist_plot_data, function(x) perc_agree_on_sign_weighted_AR4_method(x, weights=weights_rcp85))
      

      #Calculate land area where model changes are robust
      area_agree <- area(fut_mod_agr)
      area_agree[fut_mod_agr < agr_level | is.na(fut_mod_agr)] <- NA
      
      area_total <- mask(area(fut_mod_agr), fut_mod_agr)
      
      land_area[[m]][p] <- sum(values(area_agree), na.rm=TRUE)  / 
        sum(values(area_total), na.rm=TRUE)
      
      
      
      #Resample so points larger
      fut_mod_agr <- resample(fut_mod_agr, stipple_raster)
      
      
      
      #Find pixels where lag-1 autocore greated than CI
      ind    <- which(values(fut_mod_agr) >= agr_level)
      coords <- coordinates(fut_mod_agr)
      
      #Add stippling
      points(coords[ind,1], coords[ind,2], pch=20, lwd=lwd, cex=cex)
      
      #Panel number
      if (p ==1 ){
        mtext(side=3, line=0, adj=0, text="c", font=2, xpd=NA, cex=panel_cex) #panel number
        
      } else {
        mtext(side=3, line=0, adj=0, text="d", font=2, xpd=NA, cex=panel_cex) #panel number
        
      }
      
      
    } #projects
    
    dev.off()
  } #metrics
  
} #variables




