library(raster)
library(parallel)

#clear R environment
rm(list=ls(all=TRUE))


#Set path
path <- "/g/data1/w35/amu561/CMIP6_drought/" #"/srv/ccrc/data04/z3509830/CMIP6_drought//"


source(paste0(path, "/scripts/R/functions/areal_mean.R"))


#Variables
vars <- c("pr")#, "tas") #list.files(paste0(dr_path, exp[1]))



#Project and experiment names
proj <- c("Observed")

exp <- list( c("observed"))

dr_path <- list(paste0(path, "/Obs_Data/"))




#Set output directory
outdir <- paste0(path, "/Timeseries_mean_var/")
dir.create(outdir, recursive=TRUE)



#Set regions
regions <- list(Amazon=c(-78, -48, -19, 7),
                SouthAfrica = c(10, 40, -38, -15),
                #NorthernRussia =c(80, 100, 60, 75),
                Mediterranean = c(-12, 40, 28, 44),
                Australia = c(110, 155, -45, -20),
                Europe=c(-12, 30, 44, 62))


#For testing regions:                       
#plot(mean(rcp_data, na.rm=T))
#
# plot(obs_mean)
# polygon(c(10, 42, 42, 10), c(-38, -38, -12, -12))
# 


#Set number of cores
cl <- makeCluster(getOption('cl.cores', 8))

clusterExport(cl, 'areal_mean')
clusterEvalQ(cl, library(raster)) 


for (r in 1:length(regions)) {
  
  #Loop through variables
  for (v in 1:length(vars)) {
    
    
    #Loop through projects
    for (p in 1:length(proj)) { 
      
      
      #Loop through experiments
      for (e in 1:length(exp[[p]])) {
        
        #Append to outdir
        outdir_exp <- paste(outdir, proj[p], exp[[p]][e], vars[v], sep="/")
        dir.create(outdir_exp, recursive=TRUE)
        
        
        
        #Set output file name and check if it already exists
        outfile <- paste0(outdir_exp, "/Timeseries_mean_", vars[v], "_", proj[p], "_", exp[[p]][e], 
                          "_", names(regions)[r], ".csv")
        
        
        #if (file.exists(outfile)) { next }
        
        
        
        #Find models
        models <- list.files(paste(dr_path[p], sep="/"))
        
        
        
        #Initialise output matrix
        out_mat <- matrix(nrow=1, ncol=length(c(1900:2014))*12)
        
        colnames(out_mat) <- rep(c(1900:2014), each=12)
        
        
        #Save model names
        all_mod_names <- vector()
        
        
        #Loop through models
        for (mod in 1:length(models)) {
          
      
          ensemble_hist <- list.files(paste(dr_path[p], models[mod], sep="/"))
        
    
          
          print(paste0("r: ", r, " v:", v, ", p:", p, ", e: ",e, ", mod: ", mod))
          
          #Save model name
          all_mod_names <- append(all_mod_names, models[mod])
          
          hist_files <- list.files(paste(dr_path[p], models[mod], sep="/"),
                                   full.names=TRUE)
          
          
          #Load data
          hist_data <- brick(hist_files, stopIfNotEqualSpaced=FALSE)
          
          
          
          #Crop data for region
          hist_data <- crop(hist_data, extent(regions[[r]]))
          
          
          #Get time period
          hist_yrs <- as.numeric(format(as.Date(hist_data@z$time), "%Y"))
          
          
          
          # #REMOVE ONCE FIXED IN PYTHON CODE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          # #Years are written incorrectly to some python-produced files
          # #The data should be fine though so correct years here
          # if (exp[[p]][e] == "historical"  & proj[p] == "CMIP5" & data_yrs[length(data_yrs)] < 2014) {
          #   
          #   ind <- tail(which(data_yrs==2005), n=1) + 1
          #   
          #   data_yrs[ind:length(data_yrs)] <- rep(2006:2014, each=12)
          #   
          # }
          # 
          
          
          #Extract time periods
          
          if (models[mod] == "REGEN") {
            start_hist <- 1955
          } else if (models[mod] == "CRU_TS4.02") {
            start_hist <- 1901
          } else {
            start_hist <- 1900
          }
          
          
          #Get years (up to 2014)
          subset_hist_data <- hist_data[[which(hist_yrs == start_hist)[1] : tail(which(hist_yrs == 2014), n=1)]]
          
          
          #Calculate areal mean
          clusterExport(cl, 'subset_hist_data')
          
          mean_hist <- unlist(parLapply(cl, 1:nlayers(subset_hist_data), function(x) areal_mean(subset_hist_data[[x]])))
          
          
          #Append time series
          
          if (start_hist > 1900) {
            
            all_tsteps <- append(rep(NA, (start_hist-1900) *12), mean_hist)
            
          } else {
            
            all_tsteps <- mean_hist
            
          }
          
          out_mat <- rbind(out_mat, all_tsteps)
          

        } #models
        
        #Remove first row of NA
        out_mat <- out_mat[2:nrow(out_mat),]
        
        #Add rownames
        rownames(out_mat) <- all_mod_names
        
        #Write file
        write.csv(out_mat, outfile)
        
        
      } #experiments
      
    } #projects
    
  } #variables
  
} #regions

stopCluster(cl)
