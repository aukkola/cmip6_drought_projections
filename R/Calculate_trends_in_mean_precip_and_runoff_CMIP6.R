library(raster)


#clear R environment
rm(list=ls(all=TRUE))


#Set path
path <- "/g/data1/w35/amu561/CMIP6_drought/"


#Source functions
source(paste0(path,"/scripts/R/functions/trend_per_pixel.R"))

#Experiments
experiments <- c("historical", "ssp245", "ssp585")

#Variables
variables <- c("pr", "mrro", "mrros", "evspsbl")

#Set years to use for calculating trend
#(match with experiments)
years <- list(c(1950, 2014),
              c(2015, 2099),
              c(2015, 2099))



#Loop through experiments
for (e in 1:length(experiments)) {
  
  print(paste0("Experiment ", e, "/", length(experiments)))
  
  
  #Loop through variables
  for(v in 1:length(variables)) {
    
    models <- list.files(paste(path, "/CMIP6_Data/Processed_CMIP6_data/", experiments[e], 
                               variables[v], sep="/"))
    
    
    
    #Loop through models
    for (mod in 1:length(models)){
      
      print(paste0("Model ", mod, "/", length(models)))
      
      ensembles <- list.files(paste(path, "/CMIP6_Data/Processed_CMIP6_data/", experiments[e], 
                                    variables[v], models[mod], sep="/"))
      
      
      #Output directory
      outdir <- paste0(paste(path, "/Mean_trends/CMIP6/", experiments[e],
                             variables[v], models[mod], sep="/"))
      
      dir.create(outdir, recursive=TRUE)
      
      
      #Loop through ensembles
      for (ens in 1:length(ensembles)) {
        
        #Find files (may contain several ensembles)
        files <- list.files(paste(path, "/CMIP6_Data/Processed_CMIP6_data/", experiments[e], 
                                  variables[v], models[mod], ensembles[ens], sep="/"), full.names=TRUE)
        
        
        #If didn't find any files, skip
        if(length(files) == 0) next
        
        
        #Load data
        data <- brick(files, stopIfNotEqualSpaced=FALSE)
        
        #Extract desired time period
        tstamps <- names(data)
        
        start_ind <- which(grepl(years[[e]][1], tstamps))[1]
        end_ind   <- tail(which(grepl(years[[e]][2], tstamps)), n=1)
        
        
        if (length(start_ind) ==0 | length(end_ind) == 0) {
          print(paste0("Skipping ", files[f], ", time stamps not correct !!!!!"))
          next
        }
        
        
        data <- data[[start_ind:end_ind]]
        
        
        #Calculate data
        trend <- calc(data, trend_per_pixel)#_MannKen))
        
        
        #Output file name
        fname <- paste0(outdir, "/Trend_", years[[e]][1], "_", years[[e]][2], "_", basename(files))
        
        #Write output
        writeRaster(trend, fname, varname="trend", longname=paste0("trend in monthly total"), overwrite=TRUE)
        
      } #ensembles
    } #models    
  } #variables
} #experiments

