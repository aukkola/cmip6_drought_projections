library(raster)
library(RColorBrewer)
library(maptools)
library(grDevices)
library(zoo)

#clear R environment
rm(list=ls(all=TRUE))


#Set path
path <- "/g/data1/w35/amu561/CMIP6_drought/" #"/srv/ccrc/data04/z3509830/CMIP6_drought//"


#Variables
vars <- c("pr")#, "mrro") #list.files(paste0(dr_path, exp[1]))


#Set percentile and scale
percentile <- "Perc_15"

scale      <- "Scale_3"

baseline   <- "Baseline_1950_2014"


#List metrics
metrics <- c("duration", "intensity")
unit <- c("months", "mm/month")

proj <- c("CMIP6", "CMIP5")

exp <- list(c("ssp585"), c("rcp85"))


#Set regions
regions <- c("Amazon", "Australia", "Europe", "Mediterranean", "SouthAfrica")

panel_cex <- 1.0


#Colours
cols <- c(#"#8dd3c7", "#ffffb3",
          "#bebada", "#fb8072",
          "#80b1d3", "#fdb462",
          "#b3de69")



#Set output directory
outdir <- paste0(path, "/Figures/Paper_figures/")
dir.create(outdir, recursive=TRUE)



### Set up figure ###
png(paste0(outdir, "/FigureS5_ensemble_members_regional_tseries_of_drought_metrics_pr_",
           percentile, "_", scale, "_CMIP6_CMIP5.png"),
    height=9.5, width=8.27, units="in", res=400)


par(mai=c(0.2, 0.2, 0.2, 0.3))
par(omi=c(0.3, 0.6, 0.3, 0))

par(mfcol=c(length(regions), 4))



#loop through metrics
for (m in 1:length(metrics)) {
  
  
    
    #Loop through projects
    for (p in 1:length(proj)) {
      
      #Loop through regions
      for (r in 1:length(regions)){

  
      file85 <- paste0(path, "/Timeseries/", proj[p], "/", exp[[p]], "/pr/", percentile, "/", baseline, "/",
                       scale, "/", metrics[m], "/Timeseries_pr_", proj[p], "_", exp[[p]], "_", metrics[m], "_", regions[r], ".csv")
      
      #Load data
      data85 <- read.csv(file85, header=TRUE)
      
      #Work out weights
      models <- data85[,1]
      
      
      #Models with multiple ensembles
      if (proj[p] == "CMIP6") {
        
        all_models <- c("BCC-CSM2-MR", "CESM2-WACCM", "CNRM-CM6-1",
                        "CanESM5", "GFDL-CM4", "IPSL-CM6A-LR", "MIROC6",
                        "MRI-ESM2-0", "UKESM1-0-LL")
          
        subset_models <- c("CNRM-CM6-1", "UKESM1-0-LL")
        
      } else if (proj[p] == "CMIP5") {
        
        all_models <- c("bcc-csm1-1", "CESM1-WACCM", "CNRM-CM5", "CanESM2",
                        "GFDL-CM3", "IPSL-CM5A-LR", "MIROC5", "MRI-CGCM3",
                        "HadGEM2-ES")
          
        subset_models <- c("CESM1-WACCM", "CanESM2", "IPSL-CM5A-LR", 
                           "MIROC5", "HadGEM2-ES")
      }
      
      
      #Remove model column
      data85 <- data85[,2:ncol(data85)]
      
      
      #Calculate rolling mean
      rollmean85 <- apply(data85, MARGIN=1, rollmean, k=12*5, na.rm=TRUE)
      
      
      
      #Extract historical period
      hist_end <- which(colnames(data85) == "X2015") 
      
      
      #Set time vectors
      all_time <- seq.Date(as.Date("1900/1/1"), by="months", length.out=nrow(rollmean85))
      
      
      #Initialise plot
      all_mod_ind <- which(models %in% all_models)
        
      ylim <- range(rollmean85[,all_mod_ind], na.rm=TRUE)
      if(r==5 & m==2 & p==2) ylim <- c(5,35)
      
      
      plot(range(all_time), range(rollmean85[,all_mod_ind], na.rm=TRUE), type="n", 
           xlab="", ylab="", mgp=c(3, .5, 0), bty='L', ylim=ylim)
        
      #region title
      if (p==1 & m==1) {
        if (regions[r] == "SouthAfrica") {
          mtext(side=2, line=4, font=2, text="South Africa", xpd=NA, cex=panel_cex) 
          
        } else {
          mtext(side=2, line=4, font=2, text=regions[r], xpd=NA, cex=panel_cex) 
        }
      }
      
      
      #main title
      if (r==1) mtext(side=3, line=2, font=2, text=proj[p], xpd=NA, cex=panel_cex) 
      
      #unit
      if (p==1) mtext(side=2, line=2, text=unit[m], xpd=NA, cex=panel_cex) 
      
      
      #Range of all models
      all_mod_range <- apply(rollmean85[,all_mod_ind], MARGIN=1, range, na.rm=TRUE)
      
      polygon(c(all_time, rev(all_time)), 
              c(all_mod_range[1,], rev(all_mod_range[2,])),
              col=adjustcolor("grey90", alpha.f=0.8), border=NA)
      
      
      
      proj_cols <- vector()
      
      #Loop through models
      for (mod in 1:length(subset_models)) {
        
        ind <- which(models == subset_models[mod])
        
        
        mod_range <- apply(rollmean85[,ind], MARGIN=1, range, na.rm=TRUE)
        
        #CESM_WACCM starts in 1955, Infs produces
        if(any(is.infinite(mod_range))) mod_range[which(is.infinite(mod_range))] <- NA
        
        polygon(c(all_time, rev(all_time)), 
                c(mod_range[1,], rev(mod_range[2,])),
                col=adjustcolor(cols[mod], alpha.f=0.8), border=NA)
        
        
      }
      
      if (r==1) legend("topleft", legend=subset_models, fill=adjustcolor(cols[1:length(subset_models)], 
                       alpha.f=0.8), bty="n")
      
      
      if (r==length(regions)) mtext(side=1, line=2, text="Year", xpd=NA, cex=panel_cex) #x-label
      
    }
    

    # #Labels
    # mtext(side=2, line=2, padj=-10.2, text=panel_lab[r], font=2, xpd=NA, cex=panel_cex, las=2) #panel number
    # mtext(side=2, line=2, text=ylab[r], xpd=NA, cex=panel_cex) #y-label
    # mtext(side=1, line=2, text="Year", xpd=NA, cex=panel_cex) #x-label
    # 
    # 
    # #legend (split in two to align text better)
    # legend(x=as.Date("1895-01-01"), y=125, legend=c("Observed", "SSP2-4.5"), bty="n",
    #        col=c("black", col_opts[length(col_opts)-1]), lty=1, lwd=2, cex=0.8, seg.len=1)
    # 
    # legend(x=as.Date("1960-01-01"), y=125, legend=c("Historical", "SSP5-8.5"), bty="n",
    #        col=c("grey50", col_opts[2]), lty=1, lwd=2, cex=0.8, seg.len=1)
    
    
  }
  
}

dev.off()
